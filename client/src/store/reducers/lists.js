import * as actionTypes from "../actions/actionTypes";
import {updateState} from "../utility";

const initialState = {
    lists: [],
    newList: null,
    render: false,
    loading: false,
    status: null,
    error: null
};

const fetchListsStart = state => {
    return updateState(state, {newList: null, error: null, loading: true});
};

const fetchListsSuccess = (state, action) => {
    return updateState(state, {
        lists: action.payload.lists,
        loading: false,
        error: null
    });
};

const fetchListsFail = (state, action) => {
    return updateState(state, {error: action.error, loading: false});
};

const cleanupLists = state => {
    return updateState(state, {newList: null, status: null, error: null});
};

const addListStart = state => {
    return updateState(state, {error: null, loading: true});
};

const addListSuccess = (state, action) => {
    return updateState(state, {
        status: action.payload.status,
        newList: action.payload.newList,
        render: !state.render,
        loading: false,
        error: null
    });
};

const addListFail = (state, action) => {
    return updateState(state, {loading: false, error: action.error});
};

const deleteListStart = state => {
    return updateState(state, {error: null, loading: true});
};

const deleteListSuccess = (state, action) => {
    return updateState(state, {
        status: action.payload.status,
        render: !state.render,
        loading: false,
        error: null
    });
};

const deleteListFail = (state, action) => {
    return updateState(state, {error: action.error, loading: false});
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_LISTS_START:
            return fetchListsStart(state, action);
        case actionTypes.FETCH_LISTS_SUCCESS:
            return fetchListsSuccess(state, action);
        case actionTypes.FETCH_LISTS_FAIL:
            return fetchListsFail(state, action);
        case actionTypes.CLEANUP_LISTS:
            return cleanupLists(state);
        case actionTypes.ADD_LIST_START:
            return addListStart(state, action);
        case actionTypes.ADD_LIST_SUCCESS:
            return addListSuccess(state, action);
        case actionTypes.ADD_LIST_FAIL:
            return addListFail(state, action);
        case actionTypes.DELETE_LIST_START:
            return deleteListStart(state, action);
        case actionTypes.DELETE_LIST_SUCCESS:
            return deleteListSuccess(state, action);
        case actionTypes.DELETE_LIST_FAIL:
            return deleteListFail(state, action);
        default:
            return state;
    }
};

export default reducer;
