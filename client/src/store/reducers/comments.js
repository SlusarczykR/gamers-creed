import * as actionTypes from "../actions/actionTypes";
import { updateState } from "../utility";

const initialState = {
  comments: [],
  loading: false,
  status: null,
  error: null
};

const fetchCommentsSuccess = (state, action) => {
  return updateState(state, {
    comments: action.payload.comments,
    error: null
  });
};

const fetchCommentsFail = (state, action) => {
  return updateState(state, { error: action.error });
};

const addCommentStart = state => {
  return updateState(state, { error: null, loading: true });
};

const addCommentSuccess = (state, action) => {
  return updateState(state, {
    status: action.payload.status,
    loading: false,
    error: null
  });
};

const addCommentFail = (state, action) => {
  return updateState(state, { loading: false, error: action.error });
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_COMMENTS_SUCCESS:
      return fetchCommentsSuccess(state, action);
    case actionTypes.FETCH_COMMENTS_FAIL:
      return fetchCommentsFail(state, action);
    case actionTypes.ADD_COMMENT_START:
      return addCommentStart(state, action);
    case actionTypes.ADD_COMMENT_SUCCESS:
      return addCommentSuccess(state, action);
    case actionTypes.ADD_COMMENT_FAIL:
      return addCommentFail(state, action);
    default:
      return state;
  }
};

export default reducer;
