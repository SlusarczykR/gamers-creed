import * as actionTypes from "../actions/actionTypes";
import {updateState} from "../utility";

const initialState = {
    username: "",
    error: null,
    loading: false
};

const cleanupUser = state => {
    return updateState(state, {username: "", error: null});
};

const addUserStart = state => {
    return updateState(state, {username: "", error: null, loading: true});
};

const addUserSuccess = (state, action) => {
    console.log("@Payload: " + action.payload.username);
    return updateState(state, {
        username: action.payload.username,
        error: null,
        loading: false
    });
};

const addUserFail = (state, action) => {
    return updateState(state, {error: action.payload.error, loading: false});
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.ADD_USER_START:
            return addUserStart(state);
        case actionTypes.CLEANUP_USER:
            return cleanupUser(state);
        case actionTypes.ADD_USER_SUCCESS:
            return addUserSuccess(state, action);
        case actionTypes.ADD_USER_FAIL:
            return addUserFail(state, action);
        default:
            return state;
    }
};

export default reducer;
