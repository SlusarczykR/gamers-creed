import * as actionTypes from "../actions/actionTypes";
import {updateState} from "../utility";

const initialState = {
    token: null,
    username: null,
    error: null,
    loading: false
};

const cleanupAuth = state => {
    return updateState(state, {error: null});
};

const authStart = state => {
    return updateState(state, {error: null, loading: true});
};

const authSuccess = (state, action) => {
    return updateState(state, {
        token: action.payload.token,
        username: action.payload.username,
        error: null,
        loading: false
    });
};

const authFail = (state, action) => {
    return updateState(state, {error: action.error, loading: false});
};

const authLogout = (state, action) => {
    return updateState(state, {token: null, username: null});
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.AUTH_START:
            return authStart(state);
        case actionTypes.CLEANUP_AUTH:
            return cleanupAuth(state);
        case actionTypes.AUTH_SUCCESS:
            return authSuccess(state, action);
        case actionTypes.AUTH_FAIL:
            return authFail(state, action);
        case actionTypes.AUTH_LOGOUT:
            return authLogout(state, action);
        default:
            return state;
    }
};

export default reducer;
