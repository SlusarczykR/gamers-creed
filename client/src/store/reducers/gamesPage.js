import * as actionTypes from "../actions/actionTypes";
import { updateState } from "../utility";

const initialState = {
  url: "",
  items: [],
  hasMoreItems: false,
  page: 1,
  error: null
};

const initGamesPage = (state, action) => {
  return updateState(state, {
    url: action.url,
    items: [],
    hasMoreItems: true,
    page: 1,
    error: null
  });
};

const fetchGamesSuccess = (state, action) => {
  console.log(action.payload);
  return updateState(state, {
    hasMoreItems: action.payload.hasMoreItems && state.page < 5,
    items: action.payload.items,
    page: action.payload.page,
    error: null
  });
};

const fetchGamesFail = (state, action) => {
  return updateState(state, { hasMoreItems: false, error: action.error });
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.INIT_GAMES_PAGE:
      return initGamesPage(state, action);
    case actionTypes.FETCH_GAMES_SUCCESS:
      return fetchGamesSuccess(state, action);
    case actionTypes.FETCH_GAMES_FAIL:
      return fetchGamesFail(state, action);
    default:
      return state;
  }
};

export default reducer;
