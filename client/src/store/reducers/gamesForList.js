import * as actionTypes from "../actions/actionTypes";
import {updateState} from "../utility";

const initialState = {
    games: [],
    gameToBeAdd: null,
    gameUUID: null,
    render: false,
    loading: false,
    status: null,
    error: null
};

const fetchGamesForListStart = state => {
    return updateState(state, {error: null, loading: true});
};

const fetchGamesForListSuccess = (state, action) => {
    return updateState(state, {
        games: action.payload.games,
        loading: false,
        error: null
    });
};

const fetchGamesForListFail = (state, action) => {
    return updateState(state, {error: action.error, loading: false});
};

const cleanupGamesForList = state => {
    return updateState(state, {gameUUID: null, status: null, error: null});
};

const setGameToBeAdd = (state, action) => {
    return updateState(state, {gameToBeAdd: action.payload.game});
};

const addGameToListStart = state => {
    return updateState(state, {gameUUID: null, error: null, loading: true});
};

const addGameToListSuccess = (state, action) => {
    return updateState(state, {
        status: action.payload.status,
        gameUUID: action.payload.gameUUID,
        render: !state.render,
        loading: false,
        error: null
    });
};

const addGameToListFail = (state, action) => {
    return updateState(state, {loading: false, error: action.error});
};

const deleteGameFromListStart = state => {
    return updateState(state, {error: null, loading: true});
};

const deleteGameFromListSuccess = (state, action) => {
    return updateState(state, {
        status: action.payload.status,
        render: !state.render,
        loading: false,
        error: null
    });
};

const deleteGameFromListFail = (state, action) => {
    return updateState(state, {error: action.error, loading: false});
};


const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_GAMES_FOR_LIST_START:
            return fetchGamesForListStart(state, action);
        case actionTypes.FETCH_GAMES_FOR_LIST_SUCCESS:
            return fetchGamesForListSuccess(state, action);
        case actionTypes.FETCH_GAMES_FOR_LIST_FAIL:
            return fetchGamesForListFail(state, action);
        case actionTypes.CLEANUP_GAMES_FOR_LIST:
            return cleanupGamesForList(state);
        case actionTypes.SET_GAME_TO_BE_ADD:
            return setGameToBeAdd(state, action);
        case actionTypes.ADD_GAME_TO_LIST_START:
            return addGameToListStart(state, action);
        case actionTypes.ADD_GAME_TO_LIST_SUCCESS:
            return addGameToListSuccess(state, action);
        case actionTypes.ADD_GAME_TO_LIST_FAIL:
            return addGameToListFail(state, action);
        case actionTypes.DELETE_GAME_FROM_LIST_START:
            return deleteGameFromListStart(state, action);
        case actionTypes.DELETE_GAME_FROM_LIST_SUCCESS:
            return deleteGameFromListSuccess(state, action);
        case actionTypes.DELETE_GAME_FROM_LIST_FAIL:
            return deleteGameFromListFail(state, action);
        default:
            return state;
    }
};

export default reducer;
