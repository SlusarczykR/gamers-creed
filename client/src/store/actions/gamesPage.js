import * as actionTypes from "./actionTypes";
import axios from "axios";

export const fetchGames = (url, page, items) => {
  return dispatch => {
    console.log("Fetching from: " + url + `/${page}`);
    axios
      .get(url + `/${page}`)
      .then(res => {
        console.log(res.data.length);
        dispatch(
          fetchGamesSuccess({
            hasMoreItems: res.data.length > 0,
            items: items.concat(...res.data),
            page: page + 1
          })
        );
      })
      .catch(error => {
        dispatch(fetchGamesFail({ error: error.message ? error.message : "An error has occurred"}));
      });
  };
};

export const initGamesPage = url => {
  console.log("$Init url: " + url);
  return {
    type: actionTypes.INIT_GAMES_PAGE,
    url: url
  };
};

export const fetchGamesSuccess = data => {
  console.log(data);
  return {
    type: actionTypes.FETCH_GAMES_SUCCESS,
    payload: {
      hasMoreItems: data.hasMoreItems,
      items: data.items,
      page: data.page
    }
  };
};

export const fetchGamesFail = error => {
  return {
    type: actionTypes.FETCH_GAMES_FAIL,
    error: error
  };
};
