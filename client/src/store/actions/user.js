import * as actionTypes from "./actionTypes";
import axios from "axios";

export const addUser = (username, password) => {
  return dispatch => {
    const url = "http://localhost:7070/users/" + username + "/" + password;
    console.log("Post on: " + url);
    dispatch(addUserStart());
    axios
      .post(url)
      .then(res => {
        dispatch(addUserSuccess(username));
      })
      .catch(error => {
        dispatch(addUserFail(error.response ? error.response.data : "An error has occurred"));
      });
  };
};


export const cleanupUser = () => {
  return {
    type: actionTypes.CLEANUP_USER
  };
};

export const addUserStart = () => {
  return {
    type: actionTypes.ADD_USER_START
  };
};

export const addUserSuccess = username => {
  return {
    type: actionTypes.ADD_USER_SUCCESS,
    payload: {
      username: username
    }
  };
};

export const addUserFail = error => {
  return {
    type: actionTypes.ADD_USER_FAIL,
    payload: {
      error: error
    }
  };
};
