import axios from "axios";
import * as actionTypes from "./actionTypes";

export const fetchGamesForList = url => {
    return dispatch => {
        console.log("Fetching from: " + url);
        dispatch(fetchGamesForListStart());
        const token = localStorage.getItem("token");
        axios
            .get(url, {
                headers: {Authorization: "Bearer " + token}
            })
            .then(res => {
                console.log(res.data);
                dispatch(
                    fetchGamesForListSuccess({
                        games: res.data
                    })
                );
            })
            .catch(error => {
                dispatch(fetchGamesForListFail(error.response ? error.response.data : "An error has occurred"));
            });
    };
};

export const fetchGamesForListStart = () => {
    return {
        type: actionTypes.FETCH_GAMES_FOR_LIST_START
    };
};

export const fetchGamesForListSuccess = data => {
    return {
        type: actionTypes.FETCH_GAMES_FOR_LIST_SUCCESS,
        payload: {
            games: data.games
        }
    };
};

export const fetchGamesForListFail = error => {
    return {
        type: actionTypes.FETCH_GAMES_FOR_LIST_FAIL,
        error: error
    };
};

export const cleanupGamesForList = () => {
    return {
        type: actionTypes.CLEANUP_GAMES_FOR_LIST
    };
};

export const addGameToList = (url, gameList, gameId, body) => {
    return dispatch => {
        console.log("Add game to list: " + url + gameList + "/" + gameId);
        console.log(body);
        dispatch(addGameToListStart());
        const token = localStorage.getItem("token");
        axios
            .post(url + gameList + "/" + gameId, body, {
                headers: {Authorization: "Bearer " + token}
            })
            .then(res => {
                console.log(res.status);
                dispatch(
                    addGameToListSuccess({
                        status: res.status,
                        gameUUID: gameList + gameId + Math.round(Math.random() * 10)
                    })
                );
            })
            .catch(error => {
                console.log(error.response.data);
                dispatch(addGameToListFail(error.response ? error.response.data : "An error has occurred"));
            });
    };
};

export const setGameToBeAdd = game => {
    return {
        type: actionTypes.SET_GAME_TO_BE_ADD,
        payload: {
            game: game
        }
    };
};


export const addGameToListStart = () => {
    return {
        type: actionTypes.ADD_GAME_TO_LIST_START
    };
};

export const addGameToListSuccess = data => {
    return {
        type: actionTypes.ADD_GAME_TO_LIST_SUCCESS,
        payload: {
            status: data.status,
            gameUUID: data.gameUUID
        }
    };
};

export const addGameToListFail = error => {
    return {
        type: actionTypes.ADD_GAME_TO_LIST_FAIL,
        error: error
    };
};

export const deleteGameFromList = (url, gameList, gameId) => {
    return dispatch => {
        console.log("Delete game from list: " + url + gameList + "/" + gameId);
        dispatch(deleteGameFromListStart());
        const token = localStorage.getItem("token");
        axios
            .delete(url + gameList + "/" + gameId, {
                headers: {Authorization: "Bearer " + token}
            })
            .then(res => {
                console.log(res.status);
                dispatch(
                    deleteGameFromListSuccess({
                        status: res.status,
                        gameUUID: gameList + gameId + Math.round(Math.random() * 10),

                    })
                );
            })
            .catch(error => {
                dispatch(deleteGameFromListFail(error.response ? error.response.data : "An error has occurred"));
            });
    };
};

export const deleteGameFromListStart = data => {
    return {
        type: actionTypes.DELETE_GAME_FROM_LIST_START
    };
};


export const deleteGameFromListSuccess = data => {
    return {
        type: actionTypes.DELETE_GAME_FROM_LIST_SUCCESS,
        payload: {
            status: data.status,
            gameUUID: data.gameUUID
        }
    };
};

export const deleteGameFromListFail = error => {
    return {
        type: actionTypes.DELETE_GAME_FROM_LIST_FAIL,
        error: error
    };
};
