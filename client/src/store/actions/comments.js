import * as actionTypes from "./actionTypes";
import axios from "axios";

export const fetchComments = url => {
    return dispatch => {
        console.log("Fetching from: " + url);
        axios
            .get(url)
            .then(res => {
                console.log(res.data);
                dispatch(
                    fetchCommentsSuccess({
                        comments: res.data
                    })
                );
            })
            .catch(error => {
                dispatch(fetchCommentsFail({error: error.message ? error.message : "An error has occurred"}));
            });
    };
};

export const fetchCommentsSuccess = data => {
    return {
        type: actionTypes.FETCH_COMMENTS_SUCCESS,
        payload: {
            comments: data.comments
        }
    };
};

export const fetchCommentsFail = error => {
    return {
        type: actionTypes.FETCH_COMMENTS_FAIL,
        error: error
    };
};

export const addComment = (url, body) => {
    return dispatch => {
        console.log("Post on: " + url);
        dispatch(addCommentStart());
        const token = localStorage.getItem("token");
        axios
            .post(url, body, {
                headers: {
                    Authorization: "Bearer " + token,
                    "Content-Type": "text/plain"
                }
            })
            .then(res => {
                console.log(res.status);
                dispatch(
                    addCommentSuccess({
                        status: res.status
                    })
                );
            })
            .catch(error => {
                dispatch(addCommentFail({error: error.message ? error.message : "An error has occurred"}));
            });
    };
};

export const addCommentStart = () => {
    return {
        type: actionTypes.ADD_COMMENT_START
    };
};

export const addCommentSuccess = data => {
    return {
        type: actionTypes.ADD_COMMENT_SUCCESS,
        payload: {
            status: data.status
        }
    };
};

export const addCommentFail = error => {
    return {
        type: actionTypes.ADD_COMMENT_FAIL,
        error: error
    };
};
