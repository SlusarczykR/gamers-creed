import * as actionTypes from "./actionTypes";
import axios from "axios";

export const fetchLists = url => {
    return dispatch => {
        console.log("Fetching from: " + url);
        dispatch(fetchListsStart());
        const token = localStorage.getItem("token");
        axios
            .get(url, {
                headers: {Authorization: "Bearer " + token}
            })
            .then(res => {
                console.log(res.data);
                dispatch(
                    fetchListsSuccess({
                        lists: res.data
                    })
                );
            })
            .catch(error => {
                dispatch(fetchListsFail(error.response ? error.response.data : "An error has occurred"));
            });
    };
};

export const fetchListsStart = () => {
    return {
        type: actionTypes.FETCH_LISTS_START
    };
};

export const fetchListsSuccess = data => {
    return {
        type: actionTypes.FETCH_LISTS_SUCCESS,
        payload: {
            lists: data.lists
        }
    };
};

export const fetchListsFail = error => {
    return {
        type: actionTypes.FETCH_LISTS_FAIL,
        error: error
    };
};

export const cleanupLists = () => {
    return {
        type: actionTypes.CLEANUP_LISTS
    };
};

export const addList = (url, listName) => {
    return dispatch => {
        console.log("Post on: " + url + listName);
        dispatch(addListStart());
        const token = localStorage.getItem("token");
        axios
            .post(url + listName, {}, {
                headers: {Authorization: "Bearer " + token}
            })
            .then(res => {
                console.log(res.status);
                dispatch(
                    addListSuccess({
                        status: res.status,
                        newList: listName
                    })
                );
            })
            .catch(error => {
                console.log(error.response.data);
                dispatch(addListFail(error.response ? error.response.data : "An error has occurred"));
            });
    };
};

export const addListStart = () => {
    return {
        type: actionTypes.ADD_LIST_START
    };
};

export const addListSuccess = data => {
    return {
        type: actionTypes.ADD_LIST_SUCCESS,
        payload: {
            status: data.status,
            newList: data.newList
        }
    };
};

export const addListFail = error => {
    return {
        type: actionTypes.ADD_LIST_FAIL,
        error: error
    };
};

export const deleteList = url => {
    return dispatch => {
        console.log("Delete list: " + url);
        dispatch(deleteListStart());
        const token = localStorage.getItem("token");
        axios
            .delete(url, {
                headers: {Authorization: "Bearer " + token}
            })
            .then(res => {
                console.log(res.status);
                dispatch(
                    deleteListSuccess({
                        status: res.status
                    })
                );
            })
            .catch(error => {
                dispatch(deleteListFail(error.response ? error.response.data : "An error has occurred"));
            });
    };
};

export const deleteListStart = data => {
    return {
        type: actionTypes.DELETE_LIST_START
    };
};


export const deleteListSuccess = data => {
    return {
        type: actionTypes.DELETE_LIST_SUCCESS,
        payload: {
            status: data.status
        }
    };
};

export const deleteListFail = error => {
    return {
        type: actionTypes.DELETE_LIST_FAIL,
        error: error
    };
};
