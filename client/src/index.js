import React from "react";
import ReactDOM from "react-dom";
import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import {Provider} from "react-redux";
import thunk from "redux-thunk";
import "bootstrap/dist/css/bootstrap.min.css";
import "./index.css";

import App from "./containers/App";
import * as serviceWorker from "./serviceWorker";
import authReducer from "./store/reducers/auth";
import userReducer from "./store/reducers/user";
import modalReducer from "./store/reducers/modal";
import gamesPageReducer from "./store/reducers/gamesPage";
import commentReducer from "./store/reducers/comments";
import listReducer from "./store/reducers/lists";
import gamesForListReducer from "./store/reducers/gamesForList";
import {BrowserRouter as Router} from "react-router-dom";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const rootReducer = combineReducers({
    auth: authReducer,
    user: userReducer,
    modal: modalReducer,
    gamesPage: gamesPageReducer,
    comment: commentReducer,
    list: listReducer,
    gamesForList: gamesForListReducer
});

const store = createStore(
    rootReducer,
    composeEnhancers(applyMiddleware(thunk))
);

const app = (
    <Provider store={store}>
        <Router>
            <App/>
        </Router>
    </Provider>
);

ReactDOM.render(app, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
