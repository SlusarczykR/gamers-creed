import React from "react";
import {Container, Image} from "react-bootstrap";
import "./connection-error-page.css";
import serverErrorImg from "../../assets/gif/server-error.gif";

export default function ConnectionErrorPage() {
    return (
        <section id="connection-error" className="my-5">
            <Container>
                <div id="connection-error-wrapper"
                     className="d-flex flex-column justify-content-center align-items-center">
                    <div id="connection-error-img">
                        <Image src={serverErrorImg} alt="ServerErrorImg" fluid/>
                    </div>
                    <div id="header" className="text-center">
                        <h1 className="mb-4">The connection has timed out</h1>
                        <div id="info-message">
                            The site could be temporarily unavailable or too busy. Try again in a few moments<br/>
                        </div>
                    </div>
                </div>
            </Container>
        </section>
    );
}
