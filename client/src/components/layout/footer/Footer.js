import React from "react";
import "./footer.css";
import { Container } from "react-bootstrap";

export default function Footer() {
  return (
    <footer id="main-footer">
      <Container>
        <div className="c-footer-content">
          <ul className="c-t-list">
            <li>
              <a href="">Search</a>
            </li>
            <li>
              <a href="">About Us</a>
            </li>
            <li>
              <a href="">Privacy</a>
            </li>
            <li>
              <a href="">Terms</a>
            </li>
          </ul>
          <div className="c-icons">
            <i className="fab fa-facebook-f c-icon"></i>
            <i className="fab fa-twitter c-icon"></i>
            <i className="fab fa-youtube c-icon"></i>
          </div>
        </div>
        <p className="c-footer-copyright">
          Copyright &copy; 2020 Gamer's Creed. All right reserved.
        </p>
      </Container>
    </footer>
  );
}
