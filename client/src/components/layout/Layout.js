import React, {useEffect} from "react";
import {connect} from "react-redux";
import CustomNavbar from "./navbar/CustomNavbar";
import {withRouter} from "react-router-dom";
import Footer from "./footer/Footer";
import * as actions from "../../store/actions/index";
import Auth from "../pages/auth/LoginForm";
import CustomModal from "../UI/modal/CustomModal";
import RegistrationForm from "../pages/auth/RegistrationForm";
import NewListForm from "../pages/user-game-lists/new-list-form/NewListForm";
import AddGameForm from "../pages/game-details/game-details-header/game-info/game-cover/add-game-form/AddGameForm";

function Layout(props) {
    useEffect(() => {
        console.log("modalHide() => Auth: isAuth");
        if (props.isAuth) props.onHideModal();
    }, [props.isAuth]);

    useEffect(() => {
        console.log("modalHide() => Auth: history.location.key");
        props.onHideModal();
    }, [props.history.location.key]);

    let modal;
    let modalTitle;

    console.log("@ChildId " + props.childId);

    switch (props.childId) {
        case 1:
            modal = (<Auth/>);
            modalTitle = "Sign in";
            break;
        case 2:
            modal = (<RegistrationForm/>);
            modalTitle = "Registration";
            break;
        case 3:
            modal = (<NewListForm/>);
            modalTitle = "Create New List";
            break;
        case 4:
            modal = (<AddGameForm/>);
            modalTitle = "Add Game To List";
            break;
        default:
            modal = (<></>);
            modalTitle = "";
            break;
    }

    return (
        <>
            <CustomNavbar
                onShowModal={props.onShowModal}
                auth={{
                    isAuth: props.isAuth,
                    username: props.username,
                    onLogout: props.onLogout
                }}
            />
            <CustomModal
                show={props.modalShow}
                onHide={props.onHideModal}
                title={modalTitle}
            >
                {modal}
            </CustomModal>

            <main>{props.children}</main>
            <Footer/>
        </>
    );
}

const mapStateToProps = state => {
    return {
        newUser: state.user.username,
        isAuth: state.auth.token !== null,
        username: state.auth.username,
        modalShow: state.modal.modalShow,
        childId: state.modal.childId
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onAuth: (username, password) => dispatch(actions.auth(username, password)),
        onLogout: () => dispatch(actions.authLogout()),
        onShowModal: childId => dispatch(actions.modalShow(childId)),
        onHideModal: () => dispatch(actions.modalHide())
    };
};

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(Layout)
);
