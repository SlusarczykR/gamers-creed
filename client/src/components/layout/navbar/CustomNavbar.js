import React, {useState} from "react";
import {DelayInput} from "react-delay-input";
import {NavLink, withRouter} from "react-router-dom";
import {Button, Container, Form, InputGroup, Nav, Navbar, NavDropdown} from "react-bootstrap";
import "./custom-navbar.css";

const CustomNavbar = props => {
    const [value, setValue] = useState("");

    const handleSubmit = event => {
        event.preventDefault();
        value !== "" && props.history.push("/games/search/" + value);
        setValue("");
    };

    const logout = () => {
        props.auth.onLogout();
        props.history.push("/");
    }

    const authBtn = props.auth.isAuth ? (
        <NavDropdown
            title={props.auth.username}
            className="auth-nav-dropdown main-nav-link"
        >
            <NavDropdown.Item>Profile</NavDropdown.Item>
            <NavDropdown.Item as={NavLink} to={`/lists/${props.auth.username}`}>Lists</NavDropdown.Item>
            <NavDropdown.Divider/>
            <NavDropdown.Item onClick={logout}>Log out</NavDropdown.Item>
        </NavDropdown>
    ) : (
        <>
            <Nav.Link
                className="main-nav-link text-warning"
                onClick={() => props.onShowModal(1)}
            >
                Log in
            </Nav.Link>
            <Nav.Link
                className="main-nav-link"
                onClick={() => props.onShowModal(2)}
            >
                New User
            </Nav.Link>
        </>
    );

    const searchGamesForm = (
        <Form onSubmit={event => handleSubmit(event)} className="mr-3" inline>
            <InputGroup>
                <DelayInput
                    type="text"
                    value={value}
                    placeholder="Search"
                    className="form-control"
                    onChange={event => setValue(event.target.value)}
                />
                <InputGroup.Append>
                    <Button
                        className="text-center"
                        type="submit"
                        variant="outline-secondary"
                    >
                        <i className="fas fa-search"></i>
                    </Button>
                </InputGroup.Append>
            </InputGroup>
        </Form>
    );

    return (
        <Navbar bg="dark" expand="md" variant="dark">
            <Container>
                <Navbar.Brand as={NavLink} to="/" className="main-nav-link px-2">
                    Gamer's Creed
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav"/>
                <Navbar.Collapse id="responsive-navbar-nav">
                    {searchGamesForm}
                    <Nav className="mr-auto">
                        <Nav.Link as={NavLink} exact to="/" className="main-nav-link">
                            Home
                        </Nav.Link>
                        <Nav.Link as={NavLink} to="/latestNews" className="main-nav-link">
                            News
                        </Nav.Link>
                        <NavDropdown
                            title="Games"
                            className="games-nav-dropdown main-nav-link"
                        >
                            <NavDropdown.Item as={NavLink} to="/games/recentlyReleased">
                                Recently Released
                            </NavDropdown.Item>
                            <NavDropdown.Item as={NavLink} to="/games/mostAnticipated">
                                Most Anticipated
                            </NavDropdown.Item>
                            <NavDropdown.Item as={NavLink} to="/games/comingSoon">
                                Coming Soon
                            </NavDropdown.Item>
                        </NavDropdown>
                    </Nav>
                    <Nav className="ml-auto">{authBtn}</Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
};

export default withRouter(CustomNavbar);
