import uuid from "uuid";
import {Table} from "react-bootstrap";
import React from "react";

const MediaTable = props => {
    const {ItemElement, tableType, tableHead} = props;
    const tableContent = (
        <>
            {tableHead}
            <tbody>
            {props.items.map((item, index) => (
                <ItemElement key={uuid.v4()} game={item} index={index + 1}/>
            ))}
            </tbody>
        </>
    );

    let mediaTable;

    switch (tableType) {
        case "striped":
            mediaTable = (
                <Table className="media-table" striped>
                    {tableContent}
                </Table>
            );
            break;
        default:
            mediaTable = (
                <Table className="media-table" responsive>
                    {tableContent}
                </Table>
            );
            break;
    }
    return mediaTable;
}

export default MediaTable;