import React from "react";
import "./header.css";

export default function Header(props) {
  const { className, title } = props;
  return (
    <div className={className}>
      <div className="header-content">
        <h1>{title}</h1>
      </div>
      <div className="bottom-line"></div>
    </div>
  );
}
