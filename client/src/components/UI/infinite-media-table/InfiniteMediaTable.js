import React, {useEffect} from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import Loader from "./../../UI/loader/Loader";
import MediaTable from "../media-table/MediaTable";

const InfiniteMediaTable = props => {
    const {ItemElement, tableType, tableHead} = props.table;
    useEffect(() => {
        loadItems();
    }, [props.page.url]);

    const loadItems = () => {
        props.page.onfetchGames(
            props.page.url,
            props.page.page,
            props.page.items
        );
    };

    return (
        <InfiniteScroll
            dataLength={props.page.items.length}
            next={loadItems}
            hasMore={props.page.hasMoreItems}
            loader={<Loader size="md"/>}
            className="shadow-card"
        >
            <MediaTable items={props.page.items}
                        tableHead={tableHead}
                        ItemElement={ItemElement}
                        tableType={tableType}/>
        </InfiniteScroll>
    );
}

export default InfiniteMediaTable;
