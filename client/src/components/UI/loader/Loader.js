import React from "react";
import Spinner from "react-bootstrap/Spinner";

const Loader = props => {
    return (<div className="spinner-wrapper">
        <Spinner animation="border" variant="warning" size={props.size}/>
    </div>)
};

export default Loader;
