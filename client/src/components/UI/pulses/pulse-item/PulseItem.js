import React from "react";
import {Card} from "react-bootstrap";
import "./pulse-item.css";

export default function PulseItem(props) {
    const {pulse} = props;
    const title = pulse && pulse.title;
    const pulseSource = pulse && pulse.pulse_source;
    const summary = pulse && pulse.summary;
    const page = pulseSource && pulseSource.page;
    const pageUrl = page && page.url;
    const pageName = page && page.name;
    const website = pulse && pulse.website;
    const websiteUrl = website && website.url;
    const img = pulse && pulse.image;

    return (
        <Card>
            <Card.Body>
                <Card.Title>{title}</Card.Title>
                <Card.Subtitle className="mb-2 text-muted">
                    <Card.Link href={pageUrl} className="text-warning">
                        {pageName}
                    </Card.Link>
                </Card.Subtitle>
                <Card.Text>{summary}</Card.Text>
                <Card.Link href={websiteUrl} className="text-secondary">
                    Read More
                </Card.Link>
            </Card.Body>
            {img && <Card.Img variant="bottom" src={img}/>}
        </Card>
    );
}
