import React, {useEffect} from "react";
import uuid from "uuid";
import PulseItem from "./pulse-item/PulseItem";
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";
import InfiniteScroll from "react-infinite-scroll-component";
import * as actions from "../../../store/actions";
import "./pulses.css";
import Loader from "../loader/Loader";

function Pulses(props) {

    useEffect(() => {
        loadItems();
    }, [props.url]);

    const loadItems = () => {
        props.onfetchGames(
            props.url,
            props.page,
            props.items
        );
    };

    return (
        <section className="pulses-wrapper">
            <div className="cards-wrapper">
                <InfiniteScroll
                    dataLength={props.items.length}
                    next={loadItems}
                    hasMore={props.hasMoreItems}
                    loader={<Loader size="md"/>}
                >
                    {props.items.map(pulse => (
                        <PulseItem key={uuid.v4()} pulse={pulse}/>
                    ))}
                </InfiniteScroll>
            </div>
        </section>
    );
}

const mapStateToProps = state => {
    return {
        url: state.gamesPage.url,
        items: state.gamesPage.items,
        page: state.gamesPage.page,
        hasMoreItems: state.gamesPage.hasMoreItems
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onfetchGames: (url, page, items) =>
            dispatch(actions.fetchGames(url, page, items))
    };
};

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(Pulses)
);
