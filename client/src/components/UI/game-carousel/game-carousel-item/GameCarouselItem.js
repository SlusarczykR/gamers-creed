import React from "react";
import noCover from "../../../../assets/img/no-cover.jpg";
import { withRouter } from "react-router-dom";
import { Card } from "react-bootstrap";
import "./game-carousel-item.css";

function GameCarouselItem(props) {
  const { game } = props;
  const gameId = game.id;
  const title = game.name;
  const cover = game.cover && game.cover;
  const coverUrl = cover && cover.url && cover.url != "" ? cover.url : noCover;

  const getDetails = () => {
    props.history.push("/games/" + gameId);
  };

  return (
    <Card onClick={getDetails}>
      <Card.Img variant="top" src={coverUrl} />
      {props.footer && (
        <Card.Footer>
          <small className="footer-title">{title}</small>
        </Card.Footer>
      )}
    </Card>
  );
}
export default withRouter(GameCarouselItem);
