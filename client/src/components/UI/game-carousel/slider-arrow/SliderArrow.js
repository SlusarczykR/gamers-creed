import React from "react";
import "./slider-arrow.css";

export default ({to, direction, onClick}) => (
    <i
        id={`${direction}-arrow`}
        onClick={onClick}
        className={`fas fa-angle-${direction} fa-2x ${to}`}
        aria-label={to}
    />
);
