import React from "react";

export default function DetailsHeader(props) {
  const { headerUrl } = props;

  return (
    <section id="heading">
      <div
        className="heading-wrapper"
        style={{ backgroundImage: `url(${headerUrl})` }}
      >
        <div className="dark-overlay"></div>
      </div>
    </section>
  );
}
