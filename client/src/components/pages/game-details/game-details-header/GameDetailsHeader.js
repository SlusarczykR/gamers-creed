import React from "react";
import DetailsHeader from "./details-header/DetailsHeader";
import "./game-details-header.css";
import GameInfo from "./game-info/GameInfo";

export default function GameDetailsHeader(props) {
  const { game, headerUrl } = props.data;
  console.log(game);
  return (
    <>
      <DetailsHeader headerUrl={headerUrl} />
      <GameInfo game={game} />
    </>
  );
}
