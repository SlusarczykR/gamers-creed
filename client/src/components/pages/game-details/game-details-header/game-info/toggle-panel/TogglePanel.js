import React, {useState} from "react";
import "./toggle-panel.css";
import {Tab, Tabs, ListGroup} from "react-bootstrap";
import uuid from "uuid";
import {Link} from "react-router-dom";

export default function TogglePanel(props) {
    const {id, genres, summary, platforms, igdbUrl} = props.data;
    const [key, setKey] = useState("summary");
    const [showFullText, setShowFullText] = useState(false);
    let slicedSummary =
        summary && summary.length > 200 ? summary.substring(0, 200) + "..." : summary;
    const isThresholdExceeded =  slicedSummary && slicedSummary.length > 200;

    const showFullDesc = () => {
        setShowFullText(true);
    };

    return (
        <div id="panel-c">
            <Tabs id="controlled-tab" activeKey={key} onSelect={k => setKey(k)}>
                <Tab eventKey="summary" title="Summary">
                    {showFullText ? (<p>{summary}</p>) : (<><p>{slicedSummary}</p>
                        {isThresholdExceeded && isThresholdExceeded && (<Link to="#" onClick={(e) => {
                            e.preventDefault();
                            showFullDesc();
                        }} className="text-main p-1">
                            Read More
                        </Link>)} </>)}
                </Tab>
                <Tab eventKey="info" title="Info">
                    <div className="c-lists">
                        <ListGroup className="main-list">
                            <ListGroup.Item className="c-li-title">
                                <h4>Genres</h4>
                                {genres.map(genre => (
                                    <div key={uuid.v4()} className="c-li-item text-main">
                                        {genre.name}
                                    </div>
                                ))}
                            </ListGroup.Item>
                            <ListGroup.Item className="c-li-title">
                                <h4>Platforms</h4>
                                {platforms.map(platform => (
                                    <div key={uuid.v4()} className="c-li-item text-main">
                                        {platform.name}
                                    </div>
                                ))}
                            </ListGroup.Item>
                        </ListGroup>
                    </div>
                </Tab>
                <Tab eventKey="more" title="More">
                    <a href={igdbUrl} className="text-main">{igdbUrl}</a>
                </Tab>
            </Tabs>
        </div>
    );
}
