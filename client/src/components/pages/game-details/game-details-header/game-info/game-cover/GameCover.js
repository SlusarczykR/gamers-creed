import React from "react";
import IconItem from "./IconItem";
import {Button, Col, Row} from "react-bootstrap";
import "./game-cover.css";
import * as actions from "../../../../../../store/actions";
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";


const GameCover = props => {
    const {
        id,
        name,
        coverUrl,
        aggregatedRaitingCount,
        popularity,
        sum
    } = props.data;

    const gameToBeAdd = {
        id: id,
        title: name,
        coverUrl: coverUrl
    }

    const showAddToListForm = () => {
        if(props.isAuth) {
            props.onSetGameToBeAdd(gameToBeAdd);
            props.onShowModal(4);
        } else {
            props.onShowModal(1);
        }
    }

    const addToListBtn = (<Button onClick={() => {
        showAddToListForm();
    }} variant="warning" size="sm">
        <div className="d-flex align-items-center justify-content-center">
            <i className="fas fa-plus mr-2"></i>
            <span>List</span>
        </div>
    </Button>);

    return (
        <div className="cover">
            <img src={coverUrl}/>
            {addToListBtn}
            <Row>
                <Col>
                    <IconItem
                        icon="far fa-heart fa-2x"
                        title="Popularity"
                        data={popularity}
                    />
                </Col>
                <Col>
                    <IconItem icon="fas fa-plus-circle fa-2x" title="Dlcs" data={sum}/>
                </Col>
                <Col>
                    <IconItem
                        icon="far fa-star fa-2x"
                        title="Ratings"
                        data={aggregatedRaitingCount}
                    />
                </Col>
            </Row>
            <div className="c-header-title">
                <h1>{name}</h1>
            </div>
        </div>
    );
}

const mapStateToProps = state => {
    return {
        isAuth: state.auth.token !== null,
        principal: state.auth.username,
        loading: state.list.loading,
        gameLists: state.list.lists,
        render: state.list.render
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onShowModal: childId => dispatch(actions.modalShow(childId)),
        onFetchLists: url =>
            dispatch(actions.fetchLists(url)),
        onSetGameToBeAdd: game =>
            dispatch(actions.setGameToBeAdd(game))
    };
};

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(GameCover)
);
