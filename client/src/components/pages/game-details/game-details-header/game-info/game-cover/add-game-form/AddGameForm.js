import React, {useEffect, useRef, useState} from "react";
import {Alert, Button, Form} from "react-bootstrap";
import {Link, withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {ADD_GAME_TO_LIST, GAME_LISTS} from "../../../../../../../endpoints";
import uuid from "uuid";
import * as actions from "../../../../../../../store/actions/index";
import "./add-game-form.css";
import Loader from "../../../../../../UI/loader/Loader";

function AddGameForm(props) {
    const selectInput = useRef();
    const [alert, setAlert] = useState(null);
    const isListNotEmpty = props.gameLists.length > 0;

    useEffect(() => {
        return () => {
            props.onCleanupGamesForList();
        }
    }, []);

    useEffect(() => {
        if (props.isAuth) {
            props.onFetchLists(GAME_LISTS);
        }
    }, [props.principal, props.gameLists.length]);

    useEffect(() => {
        if (props.error) {
            setAlert(props.error);
        } else if (!props.error && props.gameUUID) {
            setAlert("Game has been successfully added to list");
        }
    }, [props.error, props.gameUUID]);

    const handleSubmit = e => {
        e.preventDefault();
        props.onAddGameToList(ADD_GAME_TO_LIST, selectInput.current.value, props.gameToBeAdd.id, {
            title: props.gameToBeAdd.title,
            coverUrl: props.gameToBeAdd.coverUrl
        });
    };

    const submitBtn = !props.isLoading ? (
        <Button variant="warning" type="submit" block>
            Add To List
        </Button>
    ) : (<Loader size="sm"/>);

    const errorMessage = (
        <Alert variant="warning" onClose={() => setAlert(null)} dismissible>
            <p>{alert}</p>
        </Alert>
    );

    const successMessage = (
        <Alert variant="success" onClose={() => setAlert(null)} dismissible>
            <p>{alert}</p>
        </Alert>
    );

    const alertMessage = alert && (alert == "Game has been successfully added to list" ? successMessage : errorMessage);

    const listSelect = (
        <>
            <Form.Group controlId="exampleForm.ControlSelect">
                <Form.Label>Lists</Form.Label>
                <Form.Control ref={selectInput} as="select">
                    {props.gameLists.map(item => (
                        <option key={uuid.v4()} value={item.gameList.id}>{item.gameList.name}</option>
                    ))}
                </Form.Control>
            </Form.Group>
            {submitBtn}
        </>
    );

    const newListForm = (
        <>
            {alertMessage}
            <Form id="addGameForm" onSubmit={handleSubmit}>
                {isListNotEmpty ? listSelect : (
                    <div className="my-2">
                        <span>Don't have any list yet?</span>
                        <Link to={`/lists/${props.principal}`} className="text-main ml-2">Create New List</Link>
                    </div>
                )}
            </Form>
        </>
    );
    return newListForm;
}

const mapStateToProps = state => {
    return {
        isAuth: state.auth.token !== null,
        principal: state.auth.username,
        isModalShown: state.modal.modalShow,
        error: state.gamesForList.error,
        loading: state.gamesForList.loading,
        gameToBeAdd: state.gamesForList.gameToBeAdd,
        gameUUID: state.gamesForList.gameUUID,
        render: state.gamesForList.render,
        gameLists: state.list.lists
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchLists: url =>
            dispatch(actions.fetchLists(url)),
        onAddGameToList: (url, gameList, gameId, body) =>
            dispatch(actions.addGameToList(url, gameList, gameId, body)),
        onCleanupGamesForList: () =>
            dispatch(actions.cleanupGamesForList())
    };
};

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(AddGameForm)
);
