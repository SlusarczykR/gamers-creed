import React from "react";

export default function IconItem(props) {
  const { icon, title, data } = props;

  return (
    <div className="icon-item">
      <i className={icon}></i>
      <div className="c-text-small">{title}</div>
      <div className="c-text-small">{data}</div>
    </div>
  );
}
