import React from "react";
import {Button, ButtonGroup, Dropdown, DropdownButton} from "react-bootstrap";
import "./list-dropdown.css";

const ListDropdown = props => {
    return (
        <Dropdown drop="right">
            <Dropdown.Toggle drop="right" as="div">{props.children}</Dropdown.Toggle>

            <Dropdown.Menu>
                <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
                <Dropdown.Item href="#/action-2">Another action</Dropdown.Item>
                <Dropdown.Item href="#/action-3">Something else</Dropdown.Item>
            </Dropdown.Menu>
        </Dropdown>);
}

export default ListDropdown;