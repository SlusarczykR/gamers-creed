import React from "react";
import noCover from "../../../../../assets/img/no-cover.jpg";
import {Container} from "react-bootstrap";
import {buildStyles, CircularProgressbar} from "react-circular-progressbar";
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";
import GameCover from "./game-cover/GameCover";
import TogglePanel from "./toggle-panel/TogglePanel";
import "./game-info.css";
import * as actions from "../../../../../store/actions";
import "react-circular-progressbar/dist/styles.css";

const GameInfo = props => {
    const {game} = props;
    const id = game && game.id;
    const name = game && game.name;
    const igdbUrl = game && game.url;
    const summary = game && game.summary;
    const cover = game && game.cover && game.cover.url;
    const coverUrl = cover ? cover : noCover;
    const aggregatedRaitingCount =
        game && game.aggregated_rating_count ? game.aggregated_rating_count : 0;
    const popularity =
        game && game.popularity ? parseInt(game.popularity, 10) : 0;
    const dlcs = game && game.dlcs ? game.dlcs.length : 0;
    const expansions = game && game.expansions ? game.expansions.length : 0;
    const sum = dlcs + expansions;
    const genres = game && game.genres && game.genres.length ? game.genres : [];
    const platforms =
        game && game.platforms && game.platforms.length ? game.platforms : [];
    const aggregatedRating = game && parseInt(game.aggregated_rating, 10);

    return (
        <section id="game-info">
            <Container>
                <div className="game-c-container">
                    <GameCover
                        data={{
                            id,
                            name,
                            coverUrl,
                            aggregatedRaitingCount,
                            popularity,
                            sum
                        }}
                    />
                    <TogglePanel data={{id, genres, platforms, summary, igdbUrl}}/>
                    <div
                        id="score"
                        className="d-flex flex-column align-items-start justify-content-center"
                    >
                        <CircularProgressbar
                            className="score-bar"
                            value={aggregatedRating}
                            text={aggregatedRating}
                            background={true}
                            backgroundPadding={"7"}
                            styles={buildStyles({
                                rotation: 0.25,
                                strokeLinecap: "round",
                                textSize: "24px",
                                pathTransitionDuration: 0.5,
                                pathColor: "#ffc107",
                                textColor: "#000",
                                trailColor: "#ededed",
                                backgroundColor: "#fff"
                            })}
                        />
                        <div className="text-wrapper">
                            <p className="text-muted text-center">
                                {"Based on " + aggregatedRaitingCount + " critics ratings"}
                            </p>
                        </div>
                    </div>
                </div>
            </Container>
        </section>
    );
}

const mapStateToProps = state => {
    return {
        isAuth: state.auth.token !== null,
        principal: state.auth.username,

    };
};

const mapDispatchToProps = dispatch => {
    return {
        onShowModal: childId => dispatch(actions.modalShow(childId)),
        onAddGameToList: (url, gameList, gameId, body) =>
            dispatch(actions.addGameToList(url, gameList, gameId, body))
    };
};

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(GameInfo)
);
