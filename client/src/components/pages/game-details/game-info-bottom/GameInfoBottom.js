import React, {useEffect, useState} from "react";
import {Col, Container, Row} from "react-bootstrap";
import GameInfoTable from "./game-info-table/GameInfoTable";
import Header from "../../../UI/header/Header";
import GameCarousel from "../../../UI/game-carousel/GameCarousel";
import Reviews from "./reviews/Reviews";
import "./game-info-bottom.css";
import Pulses from "./pulses/Pulses";

export default function GameInfoBottom(props) {
    const [mounted, setMounted] = useState(false);

    useEffect(() => {
        setMounted(true);
        return () => {
            setMounted(false);
        };
    }, []);

    const responsive = [
        {
            breakpoint: 1200,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 4
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3
            }
        }
    ];

    return mounted && (
        <section id="game-data">
            <Container>
                <div className="game-data-wrapper">
                    <Row>
                        <Col
                            xs={{span: 12, order: 2}}
                            sm={{span: 12, order: 2}}
                            md={{span: 12, order: 2}}
                            lg={{span: 9, order: 1}}
                        >
                            <Header
                                className="header-wrapper"
                                title="Member reviews"
                            />
                            <Reviews
                                id={props.id}
                                title={props.title}
                                isAuth={props.isAuth}
                                username={props.username}
                                comments={props.comments}
                                loading={props.loading}
                                onFetchComments={props.onFetchComments}
                                onAddComment={props.onAddComment}
                            />
                            <Header
                                className="header-wrapper"
                                title="Recommendations"
                            />
                            <GameCarousel
                                url={props.recommendationsUrl}
                                slides={5}
                                responsive={responsive}
                                footer={true}
                            />
                            <Header className="header-wrapper" title="News"/>
                            <Pulses gameId={props.id} gameTitle={props.title}/>
                        </Col>
                        <Col
                            xs={{span: 12, order: 1}}
                            sm={{span: 12, order: 1}}
                            md={{span: 12, order: 1}}
                            lg={{span: 3, order: 2}}
                            className="info-col"
                        >
                            <Header
                                className="header-wrapper"
                                title="Information"
                            />
                            <GameInfoTable id={props.id}/>
                        </Col>
                    </Row>
                </div>
            </Container>
        </section>
    );
}
