import React from "react";
import uuid from "uuid";
import PulseItem from "./../../../../UI/pulses/pulse-item/PulseItem";
import {connect} from "react-redux";
import {Link, withRouter} from "react-router-dom";
import {useHttp} from "../../../../../hooks/http";
import {GAME_PULSES} from "../../../../../endpoints";
import Loader from "../../../../UI/loader/Loader";
import * as actions from "../../../../../store/actions";
import "../../../../UI/pulses/pulses.css";

function Pulses(props) {
    const [isLoading, fetchedData] = useHttp(GAME_PULSES + props.gameId, []);
    const pulses = fetchedData ? fetchedData.data : [];

    const hasMoreItems = pulses && pulses.length >= 3;

    return isLoading ? (<Loader size="md"/>) : (
        <section className="pulses-wrapper">
            <div className="cards-wrapper">
                {pulses.map(pulse => (
                    <PulseItem key={uuid.v4()} pulse={pulse}/>
                ))}
                {hasMoreItems && <div id="link-wrapper" className="text-center">
                    <Link to={`/news/${props.gameTitle}/${props.gameId}`} className="text-main">
                        See More
                    </Link></div>}
            </div>
        </section>
    );
}

const mapStateToProps = state => {
    return {
        url: state.gamesPage.url,
        items: state.gamesPage.items,
        page: state.gamesPage.page,
        hasMoreItems: state.gamesPage.hasMoreItems
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onfetchGames: (url, page, items) =>
            dispatch(actions.fetchGames(url, page, items))
    };
};

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(Pulses)
);
