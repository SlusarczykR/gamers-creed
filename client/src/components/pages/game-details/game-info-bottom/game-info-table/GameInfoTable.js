import React, {useEffect} from "react";
import GameInfoList from "./game-info-list/GameInfoList";
import {useHttp} from "../../../../../hooks/http";
import {ListGroup} from "react-bootstrap";
import {GAME_INFO} from "../../../../../endpoints";
import "./game-info-table.css";
import Loader from "../../../../UI/loader/Loader";

export default function GameInfoTable(props) {
    const [isLoading, fetchedData] = useHttp(GAME_INFO + props.id, []);
    const game = fetchedData ? fetchedData.data : {};

    useEffect(() => {
        console.log("$$$$$$$$$$$$$$$$$$$ GameTable's info:");
        console.log(game);
    });

    const releaseDate =
        game && game.first_release_date
            ? Array.of(game.first_release_date * 1000)
            : [];
    const platforms = game && game.platforms ? game.platforms : [];
    const gameModes = game && game.game_modes ? game.game_modes : [];
    const playerPerspectives =
        game && game.player_perspectives ? game.player_perspectives : [];
    const genres = game && game.genres ? game.genres : [];
    const involvedCompanies =
        game && game.involved_companies ? game.involved_companies : [];
    const gameEngines = game && game.game_engines ? game.game_engines : [];
    const themes = game && game.themes ? game.themes : [];
    const dlcs = game && game.dlcs ? game.dlcs : [];
    const expansions = game && game.expansions ? game.expansions : [];

    return (
        <div className="information">
            {isLoading ? (<Loader size="sm"/>) : (
                <ListGroup className="lists-wrapper">
                    <ListGroup.Item>
                        <GameInfoList
                            title="Release date"
                            type="releaseDate"
                            items={releaseDate}
                        />
                    </ListGroup.Item>
                    <ListGroup.Item>
                        <GameInfoList
                            title="Platforms"
                            type="platforms"
                            items={platforms}
                        />
                    </ListGroup.Item>
                    <ListGroup.Item>
                        <GameInfoList
                            title="Game Modes"
                            type="gameModes"
                            items={gameModes}
                        />
                    </ListGroup.Item>
                    <ListGroup.Item>
                        <GameInfoList
                            title="Player Perspectives"
                            type="playerPerspectives"
                            items={playerPerspectives}
                        />
                    </ListGroup.Item>
                    <ListGroup.Item>
                        <GameInfoList title="Genres" type="genres" items={genres}/>
                    </ListGroup.Item>
                    <ListGroup.Item>
                        <GameInfoList
                            title="Involved Companies"
                            type="involvedCompanies"
                            items={involvedCompanies}
                        />
                    </ListGroup.Item>
                    <ListGroup.Item>
                        <GameInfoList
                            title="Game Engines"
                            type="gameEngines"
                            items={gameEngines}
                        />
                    </ListGroup.Item>
                    <ListGroup.Item>
                        <GameInfoList title="Themes" type="themes" items={themes}/>
                    </ListGroup.Item>
                    <ListGroup.Item>
                        <GameInfoList title="Dlcs" type="dlcs" items={dlcs}/>
                    </ListGroup.Item>
                    <ListGroup.Item>
                        <GameInfoList
                            title="Expansions"
                            type="expansions"
                            items={expansions}
                        />
                    </ListGroup.Item>
                </ListGroup>
            )}
        </div>
    );
}
