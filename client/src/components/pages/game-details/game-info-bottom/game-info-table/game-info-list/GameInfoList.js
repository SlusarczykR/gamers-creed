import React from "react";
import uuid from "uuid";
import "./game-info-list.css";

export default function GameInfoList(props) {
  const { title, type, items } = props;

  return (
    <>
      <h2>{title}</h2>
      <ul className="nested-list">
        {items.length ? (
          items.map(
            item =>
              item && (
                <li key={uuid.v4()} className="text-main">
                  {type === "involvedCompanies"
                    ? item.company && item.company.name
                    : type === "releaseDate"
                    ? new Date(item).toDateString()
                    : item.name}
                </li>
              )
          )
        ) : (
          <li key={uuid.v4()} className="text-main">
            No information
          </li>
        )}
      </ul>
    </>
  );
}
