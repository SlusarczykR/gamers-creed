import React, {useEffect, useState} from "react";
import {Button, Form} from "react-bootstrap";
import {ADD_COMMENT} from "../../../../../../endpoints";
import InputUtils from "../../../../../../utils/form/InputUtils";
import CustomInput from "../../../../../UI/form/input/CustomInput";
import "./new-review-form.css";
import Loader from "../../../../../UI/loader/Loader";

export default function NewReviewForm(props) {
    const [textAreaValue, setTextAreaValue] = useState("");
    const [textAreaValidity, setTextAreaValidity] = useState(true);
    const [textAreaTouched, setTextAreaTouched] = useState(false);

    useEffect(() => {
        setTextAreaTouched(true);
    }, [textAreaValue]);

    const handleSubmit = event => {
        event.preventDefault();
        if (InputUtils.checkValidity(textAreaValue, {required: true, minLength: 150})) {
            setTextAreaValidity(true);
            props.onAddComment(
                ADD_COMMENT + props.id,
                textAreaValue
            );
            setTextAreaValue("");
        } else {
            setTextAreaValidity(false);
        }
    };

    return (
        <div id="comment-form">
            <Form onSubmit={event => handleSubmit(event)}>
                <Form.Group controlId="review.ControlTextarea">
                    <Form.Label className="font-weight-bold">Write a review</Form.Label>
                    <CustomInput
                        type="textarea"
                        rows="6"
                        changeHandler={setTextAreaValue}
                        required={true}
                        alertMessage="Review must be at least 150 characters!"
                        invalid={!textAreaValidity}
                        touched={textAreaTouched}
                        value={textAreaValue}
                    />
                </Form.Group>
                <div className="post-btn">
                    {props.loading ? (
                        <Loader size="sm"/>
                    ) : (
                        <Button className="text-center" type="submit" variant="warning">
                            Add comment
                        </Button>
                    )}
                </div>
            </Form>
        </div>
    );
}
