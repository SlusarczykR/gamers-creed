import React, {useEffect} from "react";
import uuid from "uuid";
import NewReviewForm from "./new-review-form/NewReviewForm";
import {COMMENTS_WITH_ID} from "../../../../../endpoints";
import {Link} from "react-router-dom";
import ReviewItem from "../../../reviews/review-item/ReviewItem";
import "./reviews.css";

export default function Reviews(props) {
    const {id, title, username, onFetchComments, onAddComment} = props;
    let {comments} = props;

    useEffect(() => {
        onFetchComments(COMMENTS_WITH_ID + id);
    }, [id, props.loading]);

    const isListNotEmpty = comments.length > 0;
    const isListLong = comments.length > 3;
    comments = isListLong ? comments.slice(0, 3) : comments;

    return (
        <section id="comments" className="mb-3">
            {props.isAuth ? (
                <NewReviewForm
                    id={id}
                    username={username}
                    loading={props.loading}
                    onAddComment={onAddComment}
                    className="mb-5"
                />
            ) : (
                <div className="text-center mb-5">
                    Join the community to review your favourite games Create your account
                </div>
            )}
            {isListNotEmpty &&
            (
                <>
                    <div id="comments-wrapper" className="my-5">
                        {comments.map(comment => (
                            <ReviewItem key={uuid.v4()} comment={comment}/>
                        ))}
                    </div>
                    {isListLong && (
                        <div className="text-center my-5">
                            <Link to={`/games/reviews/${title}/${id}`} className="text-main">
                                Read more reviews
                            </Link>
                        </div>
                    )}
                </>
            )}

        </section>
    );
}
