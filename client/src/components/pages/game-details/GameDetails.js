import React, {useEffect, useState} from "react";
import {useHttp} from "../../../hooks/http";
import {connect} from "react-redux";
import {GAME_HEADER, GAME_SIMILAR_GAMES} from "../../../endpoints";
import GameDetailsHeader from "./game-details-header/GameDetailsHeader";
import MediaCarousel from "./media-carousel/MediaCarousel";
import GameInfo from "./game-info-bottom/GameInfoBottom";
import {withRouter} from "react-router-dom";
import * as actions from "../../../store/actions/index";
import Loader from "../../UI/loader/Loader";

function GameDetails(props) {
    const id = props.match.params.gameId;
    const [mounted, setMounted] = useState(false);
    const [isLoading, fetchedData] = useHttp(GAME_HEADER + id, [id]);
    const gameDetails = fetchedData ? fetchedData.data : {};
    const game = gameDetails && gameDetails.game;
    const title = game && game.name;
    const headerUrl = gameDetails && gameDetails.headerUrl;

    useEffect(() => {
        setMounted(true);
        return () => {
            setMounted(false);
        };
    }, [id]);

    return mounted && (isLoading ? (<Loader size="md"/>) : (
        <>
            <GameDetailsHeader data={{game, headerUrl}}/>
            <MediaCarousel id={id}/>
            <GameInfo
                id={id}
                title={title}
                recommendationsUrl={GAME_SIMILAR_GAMES + id}
                isAuth={props.isAuth}
                username={props.username}
                comments={props.comments}
                loading={props.loading}
                onInitGamesPage={props.onInitGamesPage}
                onFetchComments={props.onFetchComments}
                onAddComment={props.onAddComment}
                onfetchGames={props.onfetchGames}
            />
        </>
    ));
}

const mapStateToProps = state => {
    return {
        isAuth: state.auth.token !== null,
        username: state.auth.username,
        loading: state.comment.loading,
        comments: state.comment.comments
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchComments: url => dispatch(actions.fetchComments(url)),
        onAddComment: (url, body) => dispatch(actions.addComment(url, body)),
        onInitGamesPage: url => dispatch(actions.initGamesPage(url)),
        onfetchGames: (url, page, items) =>
            dispatch(actions.fetchGames(url, page, items))
    };
};

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(GameDetails)
);
