import React from "react";

export default function MediaVideoItem(props) {
    const {video} = props;
    const videoId = video && video.video_id;

    return (
        <div className="c-iframe-item">
            <iframe
                src={`https://www.youtube.com/embed/${videoId}?autoplay=0`}
                width="640"
                height="480"
                className="c-slider-item"
                frameBorder="0"
            />
        </div>
    );
}
