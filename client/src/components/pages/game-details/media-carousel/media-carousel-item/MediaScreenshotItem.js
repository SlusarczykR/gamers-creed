import React from "react";
import "./media-screen-item.css";

export default function MediaScreenshotItem(props) {
    const {screenshot} = props;
    const url = screenshot && screenshot.url;

    return (
        <div className="c-img-item">
            <img src={url} className="c-slider-item"/>
        </div>
    );
}
