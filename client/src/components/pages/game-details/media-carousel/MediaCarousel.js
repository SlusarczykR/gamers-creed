import React, {useState} from "react";
import {useHttp} from "../../../../hooks/http";
import Slider from "react-slick";
import SliderArrow from "../../../UI/game-carousel/slider-arrow/SliderArrow";
import uuid from "uuid";
import {GAME_MEDIA} from "../../../../endpoints";
import {Container} from "react-bootstrap";
import MediaScreenshotItem from "./media-carousel-item/MediaScreenshotItem";
import MediaVideoItem from "./media-carousel-item/MediaVideolItem";
import "./media-carousel.css";
import Loader from "../../../UI/loader/Loader";

export default function MediaCarousel(props) {
    const [isLoading, fetchedData] = useHttp(GAME_MEDIA + props.id, []);
    const [settings, setSettings] = useState({
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        variableWidth: true,
        adaptiveHeight: true,
        prevArrow: <SliderArrow direction="left" to="prev"/>,
        nextArrow: <SliderArrow direction="right" to="next"/>
    });
    const game = fetchedData ? fetchedData.data : {};
    const screenshots = game && game.screenshots ? game.screenshots : [];
    const videos = game && game.videos ? game.videos : [];

    return isLoading ? (
        <section id="media-carousel">
            <Container>
                <Loader size="md"/>
            </Container>
        </section>
    ) : (
        (screenshots.length || videos.length) && (
            <section id="media-carousel">
                <Container>
                    <Slider {...settings}>
                        {screenshots.length &&
                        screenshots.map(screenshot => (
                            <MediaScreenshotItem key={uuid.v4()} screenshot={screenshot}/>
                        ))}
                        {videos.length &&
                        videos.map(video => (
                            <MediaVideoItem key={uuid.v4()} video={video}/>
                        ))}
                    </Slider>
                </Container>
            </section>
        )
    );
}
