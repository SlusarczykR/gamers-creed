import React, { useEffect, useState } from "react";
import { GAME_SEARCH_FOR_TITLE } from "../../../endpoints";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import "./search-results.css";
import ResultsTable from "./results-table/ResultsTable";
import * as actions from "../../../store/actions/index";

function SearchResults(props) {
  const title = props.match.params.gameTitle;
  const [mounted, setMounted] = useState(false);

  useEffect(() => {
    setMounted(true);
    props.onInitGamesPage(GAME_SEARCH_FOR_TITLE + `/${title}`);
    return () => {
      setMounted(false);
    };
  }, [title]);

  return (
    mounted && (
      <section className="search-results">
        <div className="c-container">
          <div className="search-results-c-container">
            <h3 className="search-results-header">{`Results for ${title}:`}</h3>
            <ResultsTable
              gamesPage={{
                url: props.url,
                items: props.items,
                page: props.page,
                hasMoreItems: props.hasMoreItems,
                onfetchGames: props.onfetchGames,
                onInitGamesPage: props.onInitGamesPage
              }}
            />
          </div>
        </div>
      </section>
    )
  );
}

const mapStateToProps = state => {
  return {
    url: state.gamesPage.url,
    items: state.gamesPage.items,
    page: state.gamesPage.page,
    hasMoreItems: state.gamesPage.hasMoreItems
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onInitGamesPage: url => dispatch(actions.initGamesPage(url)),
    onfetchGames: (url, page, items) =>
      dispatch(actions.fetchGames(url, page, items))
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(SearchResults)
);
