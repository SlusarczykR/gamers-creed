import React, {useEffect} from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import uuid from "uuid";
import ResultItem from "./result-item/ResultItem";
import Loader from "../../../UI/loader/Loader";

export default function ResultsTable(props) {
    useEffect(() => {
        loadItems();
    }, [props.gamesPage.url]);

    const loadItems = () => {
        props.gamesPage.onfetchGames(
            props.gamesPage.url,
            props.gamesPage.page,
            props.gamesPage.items
        );
    };

    return (
        <InfiniteScroll
            dataLength={props.gamesPage.items.length}
            next={loadItems}
            hasMore={props.gamesPage.hasMoreItems}
            loader={<Loader size="md"/>}
        >
            {props.gamesPage.items.map(game => (
                <ResultItem key={uuid.v4()} game={game}/>
            ))}
        </InfiniteScroll>
    );
}
