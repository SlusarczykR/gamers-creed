import React from "react";
import { Link } from "react-router-dom";
import noCover from "../../../../../assets/img/no-cover.jpg";
import "./result-item.css";

export default function ResultItem(props) {
  const { game } = props;
  const gameId = game && game.id;
  const name = game && game.name;
  const cover = game && game.cover;
  const coverUrl = cover && cover.url && cover.url != "" ? cover.url : noCover;

  return (
    <div className="c-t-item d-flex align-items-start justify-content-start">
      <img src={coverUrl} className="c-card-img" />
      <Link to={`/games/${gameId}`} className="text-main">
        {name}
      </Link>
    </div>
  );
}
