import React from "react";
import * as uuid from "uuid";
import {Badge} from "react-bootstrap";
import {Link, withRouter} from "react-router-dom";
import {connect} from "react-redux";
import "./game-list-item.css";
import {GAME_LISTS} from "../../../../endpoints";
import * as actions from "../../../../store/actions";

function GameListItem(props) {
    const {gameList, gameListSize} = props.gameListReponse;
    const gameListId = gameList && gameList.id;
    const gameListName = gameList && gameList.name;
    const isNotEmpty = gameListSize && gameListSize > 0;

    const deleteGameList = () => {
        props.onDeleteList(GAME_LISTS + "/" + gameListId);
    };

    const list = (
        <tr className="list-item-row" key={uuid.v4()}>
            <td className="item-td d-flex align-items-center justify-content-between py-2">
                <div className="title-wrapper mr-3">
                    <Link to={`/lists/${props.username}/${gameListName}/${gameListId}`}
                          className="text-main mr-2">
                        {gameListName}
                    </Link>
                    <Badge variant={isNotEmpty ? "warning" : "secondary"}>{gameListSize}</Badge>
                </div>
                <Link to="#" onClick={(e) => {
                    e.preventDefault();
                    deleteGameList();
                }} className="text-danger">
                    <i className="fas fa-trash"/>
                </Link>
            </td>
        </tr>
    );

    return list;
}

const mapStateToProps = state => {
    return {
        loading: state.gamesForList.loading,
        games: state.gamesForList.games,
        render: state.gamesForList.render
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onDeleteList: url =>
            dispatch(actions.deleteList(url)),
        onDeleteGameFromList: (url, gameListId, gameId) =>
            dispatch(actions.deleteGameFromList(url, gameListId, gameId))
    };
};

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps,
    )(GameListItem)
);
