import React, {useEffect, useState} from "react";
import {connect} from "react-redux";
import {Alert, Button, Form} from "react-bootstrap";
import {withRouter} from "react-router-dom";
import * as actions from "../../../../store/actions/index";
import {GAME_LISTS} from "../../../../endpoints";
import CustomInput from "../../../UI/form/input/CustomInput";
import InputUtils from "../../../../utils/form/InputUtils";
import Loader from "../../../UI/loader/Loader";

function NewListForm(props) {
    const [name, setName] = useState("");
    const [alert, setAlert] = useState(null);
    const [nameInputValidity, setNameInputValidity] = useState(true);
    const [nameInputTouched, setNameInputTouched] = useState(false);

    useEffect(() => {
        return () => {
            props.onCleanupLists();
        }
    }, []);

    useEffect(() => {
        if (props.error) {
            setAlert(props.error);
        } else if (!props.error && props.newList) {
            setAlert("List has been successfully created");
        }
    }, [props.error, props.newList]);

    useEffect(() => {
        setNameInputTouched(true);
    }, [name]);

    const handleSubmit = e => {
        e.preventDefault();
        if (InputUtils.checkValidity(name, {required: true, minLength: 3, maxLength: 20})) {
            props.onAddList(GAME_LISTS + "/", name.trim());
            setNameInputValidity(true);
        } else {
            setNameInputValidity(false);
        }
    };

    const submitBtn = !props.isLoading ? (
        <Button variant="warning" type="submit" block>
            Create List
        </Button>
    ) : (
        <Loader size="sm"/>
    );

    const errorMessage = (
        <Alert variant="danger" onClose={() => setAlert(null)} dismissible>
            <p>{alert}</p>
        </Alert>
    );

    const successMessage = (
        <Alert variant="success" onClose={() => setAlert(null)} dismissible>
            <p>{alert}</p>
        </Alert>
    );

    const alertMessage = alert && (alert === "List has been successfully created" ? successMessage : errorMessage);

    const newListForm = (
        <>
            {alertMessage}
            <Form onSubmit={handleSubmit}>
                <Form.Group controlId="formBasicEmail">
                    <Form.Label className="font-weight-bold">Name</Form.Label>
                    <CustomInput
                        type="text"
                        placeholder="Name"
                        changeHandler={setName}
                        required={true}
                        alertMessage="Name of te list must be between 3 and 20 characters!"
                        invalid={!nameInputValidity}
                        touched={nameInputTouched}
                    />
                </Form.Group>
                {submitBtn}
            </Form>
        </>
    );
    return newListForm;
}

const mapStateToProps = state => {
    return {
        isAuth: state.auth.token !== null,
        principal: state.auth.username,
        error: state.list.error,
        loading: state.list.loading,
        newList: state.list.newList
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onAddList: (url, name) =>
            dispatch(actions.addList(url, name)),
        onCleanupLists: () =>
            dispatch(actions.cleanupLists())
    };
};

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(NewListForm)
);
