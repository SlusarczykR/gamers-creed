import React, {useEffect, useState} from "react";
import {Button, Container, Table} from "react-bootstrap";
import Header from "../../UI/header/Header";
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";
import {GAME_LISTS} from "../../../endpoints";
import * as actions from "../../../store/actions/index";
import * as uuid from "uuid";
import GameListItem from "./game-list-item/GameListItem";
import "./user-game-list.css";

function UserGameLists(props) {
    const username = props.match.params.username;
    const [mounted, setMounted] = useState(false);
    const isGameListsNotEmpty = props.gameLists && props.gameLists.length > 0;

    useEffect(() => {
        props.onFetchLists(GAME_LISTS);
        setMounted(true);
        return () => {
            setMounted(false);
        };
    }, [props.principal, username, props.render]);

    const createListButton = (<div onClick={() => props.onShowModal(3)} className="mb-5 text-right">
        <Button variant="warning" className="ml-auto">
            <div className="d-flex align-items-center justify-content-center">
                <i className="fas fa-plus mr-2"></i>
                <span>New List</span>
            </div>
        </Button>
    </div>)

    const listsTable = (
        <div id="lists-table-wrapper" className="shadow-card">
            <Table id="lists-table" className="table table-hover">
                <thead>
                <tr>
                    <th>Title</th>
                </tr>
                </thead>
                <tbody>
                {isGameListsNotEmpty ? props.gameLists.map((gameListReponse, index) => (
                    <GameListItem key={uuid.v4()} gameListReponse={gameListReponse} username={username} index={index}/>
                )) : <tr>
                    <td className="text-center text-main">You haven't created any list yet</td>
                </tr>}
                </tbody>
            </Table>
        </div>
    );

    return mounted && (
        <section id="user-lists" className="my-5">
            <Container>
                <Header
                    className="header-wrapper"
                    title={username + "'s lists"}
                />
                {props.isAuth && (username === props.principal) ?
                    (<>
                        {createListButton}
                        {listsTable}
                    </>) :
                    (<div className="text-center text-main  my-5">You are not supposed to see this content</div>)}
            </Container>
        </section>
    );
}

const mapStateToProps = state => {
    return {
        isAuth: state.auth.token !== null,
        principal: state.auth.username,
        loading: state.list.loading,
        gameLists: state.list.lists,
        render: state.list.render
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onShowModal: childId => dispatch(actions.modalShow(childId)),
        onFetchLists: url =>
            dispatch(actions.fetchLists(url))
    };
};

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(UserGameLists)
);