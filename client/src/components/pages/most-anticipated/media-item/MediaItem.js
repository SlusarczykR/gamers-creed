import React from "react";
import { Media, Button } from "react-bootstrap";
import { Link, withRouter } from "react-router-dom";
import noCover from "../../../../assets/img/no-cover.jpg";

function MediaItem(props) {
  const { game } = props;
  const gameId = game && game.id;
  const name = game && game.name;
  const cover = game && game.cover && game.cover.url;
  const coverUrl = cover ? cover : noCover;
  const timestamp = game.first_release_date;
  const date = new Date(timestamp * 1000);
  const formattedDate =
    date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear();

  const getDetails = () => {
    props.history.push("/games/" + gameId);
  };

  return (
    <tr>
      <td>
        <p className="text-muted font-weight-bold">{props.index}</p>
      </td>
      <td className="item-td d-flex align-items-center justify-content-between">
        <Media className="media-item d-flex align-items-center justify-content-center">
          <img
            height="40px"
            width="40px"
            className="mr-3"
            src={coverUrl}
            alt="Table item"
            onClick={getDetails}
          />
          <Media.Body>
            <Link to={`/games/${gameId}`} className="text-main">
              {name}
            </Link>
            <div>
              <small className="text-muted">{formattedDate}</small>
            </div>
          </Media.Body>
        </Media>
      </td>
    </tr>
  );
}

export default withRouter(MediaItem);
