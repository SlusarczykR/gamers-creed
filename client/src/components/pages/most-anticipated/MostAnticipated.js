import React, {useEffect, useState} from "react";
import {Container} from "react-bootstrap";
import Header from "../../UI/header/Header";
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {MOST_POPULAR_GAMES} from "../../../endpoints";
import * as actions from "../../../store/actions/index";
import InfiniteMediaTable from "../../UI/infinite-media-table/InfiniteMediaTable";
import MediaItem from "./media-item/MediaItem";

function MostAnticipated(props) {
    const [mounted, setMounted] = useState(false);

    useEffect(() => {
        setMounted(true);
        props.onInitGamesPage(MOST_POPULAR_GAMES);
        return () => {
            setMounted(false);
        };
    }, []);

    const tableHead = (
        <thead>
        <tr>
            <th>#</th>
            <th>Title</th>
        </tr>
        </thead>
    );

    return (
        mounted && (
            <Container className="most-anticipated-wrapper my-5">
                <Header
                    className="header-wrapper"
                    title="Most Anticipated"
                />
                <InfiniteMediaTable
                    page={{
                        url: props.url,
                        items: props.items,
                        page: props.page,
                        hasMoreItems: props.hasMoreItems,
                        onfetchGames: props.onfetchGames,
                        onInitGamesPage: props.onInitGamesPage
                    }}
                    table={{
                        tableHead: tableHead,
                        ItemElement: MediaItem,
                        tableType: "striped"
                    }}
                />
            </Container>
        )
    );
}

const mapStateToProps = state => {
    return {
        url: state.gamesPage.url,
        items: state.gamesPage.items,
        page: state.gamesPage.page,
        hasMoreItems: state.gamesPage.hasMoreItems
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onInitGamesPage: url => dispatch(actions.initGamesPage(url)),
        onfetchGames: (url, page, items) =>
            dispatch(actions.fetchGames(url, page, items))
    };
};

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(MostAnticipated)
);
