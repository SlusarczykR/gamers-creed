import React from "react";
import {withRouter} from "react-router-dom";
import {Container} from "react-bootstrap";
import {RECOMMENDED_GAMES} from "../../../endpoints";
import Header from "../../UI/header/Header";
import Articles from "./articles/Articles";
import GameCarousel from "../../UI/game-carousel/GameCarousel";
import News from "./news/News";
import GamesTable from "./games-table/GamesTable";
import "./home.css";

function Home() {
    const responsive = [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 4
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3
            }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        }
    ];

    return (
        <section id="home-wrapper">
            <Container>
                <Header
                    className="header-wrapper"
                    title="Top Games Right Now"
                />
                <Articles/>
                <Header
                    className="header-wrapper"
                    title="Recommendations"
                />
                <GameCarousel
                    url={RECOMMENDED_GAMES}
                    slides={5}
                    responsive={responsive}
                    footer={true}
                />
                <Header
                    className="header-wrapper"
                    title="Recently News"
                />
                <News/>
                <GamesTable/>
            </Container>
        </section>
    );
}

export default withRouter(Home);
