import React from "react";
import { withRouter, Link } from "react-router-dom";
import { Media } from "react-bootstrap";
import noImage from "../../../../../../assets/img/no-cover.jpg";
import "./table-item.css";

function TableItem(props) {
  const { item, type } = props;
  const game = type === "game" ? item : item.game;
  const gameId = game && game.id;
  const name = game && game.name;
  const cover = game && game.cover && game.cover.url;
  const coverUrl = cover ? cover : noImage;
  const timestamp = type === "game" ? game.first_release_date : item.date;
  const date = new Date(timestamp * 1000);
  const formattedDate =
    date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear();

  const getDetails = () => {
    props.history.push("/games/" + gameId);
  };

  return (
    <Media className="c-t-item">
      <img
        className="c-card-img mr-3"
        src={coverUrl}
        alt="Table item"
        onClick={getDetails}
      />
      <Media.Body>
        <div className="c-t-item-content">
          <Link to={`games/${gameId}`} className="text-main">{name}</Link>
          <small className="text-muted">{formattedDate}</small>
        </div>
      </Media.Body>
    </Media>
  );
}
export default withRouter(TableItem);
