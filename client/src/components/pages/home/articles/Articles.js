import React from "react";
import {useHttp} from "../../../../hooks/http";
import uuid from "uuid";
import {POPULAR_GAMES} from "../../../../endpoints";
import ArticleItem from "./article-item/ArticleItem";
import "./articles.css";
import Loader from "../../../UI/loader/Loader";

export default function Articles() {
    const [isLoading, fetchedData] = useHttp(POPULAR_GAMES, []);
    const popularGames = fetchedData ? fetchedData.data : [];
    const randomSelectedGame = popularGames && popularGames[Math.floor(Math.random() * popularGames.length)];
    const randomSelectedVideo = randomSelectedGame && randomSelectedGame.videos && randomSelectedGame.videos.length > 0 &&
        randomSelectedGame.videos[Math.floor(Math.random() * randomSelectedGame.videos.length)];
    const videoId = randomSelectedVideo ? randomSelectedVideo.video_id : "";

    return (
        <section id="articles">
            {isLoading ? (
                <Loader size="md"/>
            ) : (
                <>
                    <div className="articles-c-container">
                        {popularGames.map(game => (
                            <ArticleItem key={uuid.v4()} game={game}/>
                        ))}
                    </div>
                    <div className="c-iframe-wrapper">
                        <iframe
                            width="640"
                            height="480"
                            src={"https://www.youtube.com/embed/" + videoId + "?autoplay=1"}
                            className="c-slider-item"
                            allow="autoplay"
                            frameBorder="0"
                        />
                    </div>
                </>
            )}
        </section>
    );
}
