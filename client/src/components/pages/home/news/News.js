import React from "react";
import {useHttp} from "../../../../hooks/http";
import {PULSES} from "../../../../endpoints";
import uuid from "uuid";
import Pulse from "./news-pulse/Pulse";
import "./news.css";
import Loader from "../../../UI/loader/Loader";

export default function News() {
    const [isLoading, fetchedData] = useHttp(PULSES, []);
    const pulses = fetchedData ? fetchedData.data : [];

    return (
        <section className="news">
            {isLoading ? (
                <Loader size="md"/>
            ) : (
                <div className="news-c-container">
                    {pulses.map(pulse => (
                        <Pulse key={uuid.v4()} pulse={pulse}/>
                    ))}
                </div>
            )}
        </section>
    );
}
