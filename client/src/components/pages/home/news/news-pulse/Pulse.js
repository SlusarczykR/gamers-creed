import React from "react";
import {Card} from "react-bootstrap";
import noImage from "../../../../../assets/img/no-image.png";
import "./pulse.css";

export default function Pulse(props) {
    const {pulse} = props;
    const pulseImage = pulse.image ? pulse.image : noImage;
    const pulseSourcePage = pulse.pulse_source && pulse.pulse_source.page;
    const pulseSourceUrl = pulseSourcePage.url;
    const pulseSourceName = pulseSourcePage.name;
    const websiteUrl = pulse.website && pulse.website.url;
    const author = pulse.author ? (pulse.author !== "" ? pulse.author : "Unknown") : "Unknown";

    return (
        <Card>
            <Card.Img variant="top" src={pulseImage}/>
            <Card.Body>
                <Card.Subtitle>
                    <a className="pulse-source" href={pulseSourceUrl}>{pulseSourceName}</a>
                </Card.Subtitle>
                <Card.Text>
                    <a href={websiteUrl} className="d-block">
                        {pulse.summary}
                    </a>
                    <small className="text-muted">{`By: ${author}`}</small>
                </Card.Text>
            </Card.Body>
        </Card>
    );
}
