import React from "react";
import uuid from "uuid";
import {Media} from "react-bootstrap";
import {Link, withRouter} from "react-router-dom";
import {DELETE_GAME_FROM_LIST} from "../../../../endpoints";
import * as actions from "../../../../store/actions";
import {connect} from "react-redux";

function GameItem(props) {
    const {game, gameListId} = props;

    const deleteGameFromList = (gameListId, gameId) => {
        props.onDeleteGameFromList(DELETE_GAME_FROM_LIST, gameListId, gameId)
    }

    const listItem = (
        <tr key={uuid.v4()}>
            <td className="item-td">
                <div className="d-flex align-items-center justify-content-between px-3 my-1">
                    <Media className="media-item d-flex align-items-start justify-content-center">
                        <img
                            height="40px"
                            width="40px"
                            className="mr-3"
                            src={game.coverUrl}
                            alt="Media item"
                        />
                        <Media.Body>
                            <Link to={`/games/${game.id.gameId}`}
                                  className="text-main">
                                {game.title}
                            </Link>
                        </Media.Body>
                    </Media>
                    <Link to="#" onClick={(e) => {
                        e.preventDefault();
                        deleteGameFromList(gameListId, game.id.gameId)
                    }}
                          className="text-danger p-1">
                        <i className="fas fa-trash"></i>
                    </Link>
                </div>
            </td>
        </tr>
    );

    return listItem;
}

const mapStateToProps = state => {
    return {
        games: state.gamesForList.games,
        render: state.gamesForList.render
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onDeleteGameFromList: (url, gameListId, gameId) =>
            dispatch(actions.deleteGameFromList(url, gameListId, gameId))
    };
};

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(GameItem)
);