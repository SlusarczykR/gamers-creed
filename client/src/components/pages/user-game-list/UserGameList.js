import React, {useEffect} from "react";
import {Container, Table} from "react-bootstrap";
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {GAMES_FOR_LIST} from "../../../endpoints";
import uuid from "uuid";
import ListTableItem from "./game-item/GameItem";
import Header from "../../UI/header/Header";
import * as actions from "../../../store/actions";
import "./user-game-list.css";

const UserGameList = props => {
    const username = props.match.params.username;
    const gameListId = props.match.params.listId;
    const gameListName = props.match.params.listName;
    const gameListLength = props.games && props.games.length;

    useEffect(() => {
        console.log("@UserGameList");
        props.onFetchGamesForList(GAMES_FOR_LIST + gameListId);
    }, [props.principal, username, props.render]);

    const gamesTable = (
        <div id="list-table-wrapper" className="shadow-card">
            <Table className="media-table" responsive>
                <thead>
                <tr>
                    <th>Title</th>
                </tr>
                </thead>
                <tbody>
                {gameListLength ? props.games.map(game => (
                    <ListTableItem key={uuid.v4()} game={game} gameListId={gameListId}/>
                )) : (<tr>
                    <td className="text-center text-main">The list does not contain any games</td>
                </tr>)}
                </tbody>
            </Table>
        </div>
    );

    const gameListTable = (
        <Container className="coming-soon-wrapper my-5">
            <Header className="header-wrapper" title={gameListName + " List"}/>
            {props.isAuth && (username === props.principal) ? gamesTable :
                (<div className="text-center text-main  my-5">You are not supposed to see this content</div>)}
        </Container>
    );
    return gameListTable;
}

const mapStateToProps = state => {
    return {
        isAuth: state.auth.token !== null,
        principal: state.auth.username,
        games: state.gamesForList.games,
        render: state.gamesForList.render
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchGamesForList: url => dispatch(actions.fetchGamesForList(url)),
        onDeleteGameFromList: (url, gameListId, gameId) =>
            dispatch(actions.deleteGameFromList(url, gameListId, gameId))
    };
};

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(UserGameList)
);