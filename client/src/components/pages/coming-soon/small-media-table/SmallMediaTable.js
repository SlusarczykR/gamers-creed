import React from "react";
import {useHttp} from "../../../../hooks/http";
import "./small-media-table.css";
import MediaTable from "../../../UI/media-table/MediaTable";
import MediaItem from "./small-table-item/SmallTableItem";
import Loader from "../../../UI/loader/Loader";

function SmallMediaTable(props) {
    const [isLoading, fetchedData] = useHttp(props.url, [props.url]);
    const comingSoonGames = fetchedData ? fetchedData.data : [];
    const slicedComingSoonGames = comingSoonGames.length > 5 ? comingSoonGames.slice(0, 5) : comingSoonGames;

    const tableHead = (
        <thead>
        <tr>
            <th>{props.headerTitle}</th>
        </tr>
        </thead>
    );

    const table = (
        <MediaTable
            tableHead={tableHead}
            ItemElement={MediaItem}
            items={slicedComingSoonGames}/>
    );

    return isLoading ? (<Loader size="sm"/>) : table
}

export default SmallMediaTable;
