import React, {useEffect, useState} from "react";
import {Col, Container, Row} from "react-bootstrap";
import Header from "../../UI/header/Header";
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {COMING_SOON_GAMES} from "../../../endpoints";
import SmallMediaTable from "./small-media-table/SmallMediaTable";
import * as actions from "../../../store/actions/index";
import NavItems from "./nav-items/NavItems";
import InfiniteMediaTable from "../../UI/infinite-media-table/InfiniteMediaTable";
import MediaItem from "./media-item/MediaItem";

function ComingSoon(props) {
    const [mounted, setMounted] = useState(false);

    useEffect(() => {
        setMounted(true);
        props.onInitGamesPage(COMING_SOON_GAMES + "/all/(6,48,49,130,9,12,37,46,39,34)");
        return () => {
            setMounted(false);
        };
    }, []);

    const tableHead = (
        <thead>
        <tr>
            <th>All upcoming games</th>
        </tr>
        </thead>
    );

    const weekTable = (
        <SmallMediaTable
            headerTitle="Upcoming 7 days"
            url={props.url.replace("all", "week") + "/1"}
        />
    );

    const twoWeeksTable = (
        <SmallMediaTable
            headerTitle="Upcoming 14 days"
            url={props.url.replace("all", "twoWeeks") + "/1"}
        />
    );

    const mainTable = (
        <InfiniteMediaTable
            page={{
                url: props.url,
                items: props.items,
                page: props.page,
                hasMoreItems: props.hasMoreItems,
                onfetchGames: props.onfetchGames,
                onInitGamesPage: props.onInitGamesPage
            }}
            table={{
                tableHead: tableHead,
                ItemElement: MediaItem
            }}
        />
    );

    return (
        mounted && (
            <Container className="coming-soon-wrapper my-5">
                <Header className="header-wrapper" title="Coming Soon"/>
                <NavItems
                    givenUrl={COMING_SOON_GAMES}
                    gamesPage={{
                        url: props.url,
                        onInitGamesPage: props.onInitGamesPage
                    }}
                />
                <Row className="mb-5 justify-content-between">
                    <Col sm={12} md={6} className="small-table-wrapper">
                        {weekTable}
                    </Col>
                    <Col sm={12} md={6} className="small-table-wrapper">
                        {twoWeeksTable}
                    </Col>
                </Row>
                <Row>
                    <Col>
                        {mainTable}
                    </Col>
                </Row>
            </Container>
        )
    );
}

const mapStateToProps = state => {
    return {
        url: state.gamesPage.url,
        items: state.gamesPage.items,
        page: state.gamesPage.page,
        hasMoreItems: state.gamesPage.hasMoreItems
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onInitGamesPage: url => dispatch(actions.initGamesPage(url)),
        onfetchGames: (url, page, items) =>
            dispatch(actions.fetchGames(url, page, items))
    };
};

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(ComingSoon)
);
