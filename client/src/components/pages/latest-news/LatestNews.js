import React, {useEffect, useState} from "react";
import {Container} from "react-bootstrap";
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {GAME_PULSES, LATEST_NEWS} from "../../../endpoints";
import Header from "../../UI/header/Header";
import * as actions from "../../../store/actions";
import Pulses from "../../UI/pulses/Pulses";
import "./lates-news.css";

const LatestNews = props => {
    const [mounted, setMounted] = useState(false);
    const gameId = props.match.params.gameId;
    const gameTitle = props.match.params.gameTitle;
    const header = gameTitle ? `${gameTitle} News` : "Latest News";

    useEffect(() => {
        setMounted(true);
        initSourceUrl();
        return () => {
            setMounted(false);
        };
    }, [gameId]);

    const initSourceUrl = () => {
        let url;
        if (gameId) {
            url = GAME_PULSES + gameId;
        } else {
            url = LATEST_NEWS;
        }
        props.onInitGamesPage(url);
    }

    const latestPulses = (
        <Container className="latest-news-wrapper my-5">
            <Header className="header-wrapper" title={header}/>
            <Pulses/>
        </Container>
    );
    return mounted && latestPulses;
}

const mapStateToProps = state => {
    return {
        url: state.gamesPage.url
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onInitGamesPage: url => dispatch(actions.initGamesPage(url))
    };
};

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(LatestNews)
);