import React, {useState} from "react";
import {Col, Row, Tab} from "react-bootstrap";
import "./review-item.css";
import dateFormat from "dateformat";
import {Link} from "react-router-dom";

export default function ReviewItem(props) {
    const {comment} = props;
    const createdAt = new Date(comment.createdAt);
    const formattedDate = dateFormat(createdAt, "dd-mm-yyyy h:MM TT");
    const [showFullText, setShowFullText] = useState(false);
    const isThresholdExceeded = comment.body && comment.body.length > 300;
    let slicedBody = isThresholdExceeded &&
    isThresholdExceeded ? comment.body.substring(0, 300) + "..." : comment.body;

    const showFullDesc = () => {
        setShowFullText(true);
    };

    console.log("@Review");
    console.log(comment);

    return (
        <article className="review mt-5">
            <Row>
                <Col xs={3}>
                    <div id="user-info">
                        <div className="text-main">{comment.id.user.username}</div>
                        <small className="text-muted">{formattedDate}</small>
                    </div>
                </Col>
                <Col xs={9}>
                    <div className="review-body">
                      {showFullText ? (<p>{comment.body}</p>) : (<><p>{slicedBody}</p>
                        {isThresholdExceeded && isThresholdExceeded && (<Link to="#" onClick={(e) => {
                          e.preventDefault();
                          showFullDesc();
                        }} className="text-main p-1">
                          Read More
                        </Link>)} </>)}
                    </div>
                </Col>
            </Row>
        </article>
    );
}
