import React, {useEffect} from "react";
import uuid from "uuid";
import {connect} from "react-redux";
import {COMMENTS_WITH_ID} from "../../../endpoints";
import ReviewItem from "./review-item/ReviewItem";
import {withRouter} from "react-router-dom";
import * as actions from "../../../store/actions/index";
import {Container} from "react-bootstrap";
import Header from "../../UI/header/Header";
import "./user-reviews.css";
import Loader from "../../UI/loader/Loader";

function UserReviews(props) {
    const id = props.match.params.gameId;
    const title = props.match.params.gameTitle;

    useEffect(() => {
        props.onFetchComments(COMMENTS_WITH_ID + id);
    }, [id]);

    return props.loading ? (<Loader size="md"/>) : (
        <Container className="reviews-container mt-5">
            <Header className="header-wrapper" title={`Reviews for ${title}`}/>
            <section id="reviews">
                <div className="reviews-wrapper">
                    {props.comments.map(comment => (
                        <ReviewItem key={uuid.v4()} comment={comment}/>
                    ))}
                </div>
            </section>
        </Container>
    );
}

const mapStateToProps = state => {
    return {
        loading: state.comment.loading,
        comments: state.comment.comments
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchComments: url => dispatch(actions.fetchComments(url))
    };
};

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(UserReviews)
);
