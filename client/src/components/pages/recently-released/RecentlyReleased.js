import React, {useEffect, useState} from "react";
import {Container} from "react-bootstrap";
import NavItems from "./nav-items/NavItems";
import Header from "../../UI/header/Header";
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {RECENTLY_RELEASED_GAMES} from "../../../endpoints";
import InfiniteMediaTable from "../../UI/infinite-media-table/InfiniteMediaTable";
import * as actions from "../../../store/actions/index";
import "./recently-released.css";
import MediaItem from "./media-item/MediaItem";

function RecentlyReleased(props) {
    const [mounted, setMounted] = useState(false);

    useEffect(() => {
        setMounted(true);
        props.onInitGamesPage(RECENTLY_RELEASED_GAMES + "/platform/(6,48,49,130,9,12,37,46,39,34)");
        return () => {
            setMounted(false);
        };
    }, []);

    const tableHead = (
        <thead>
        <tr>
            <th>Title</th>
        </tr>
        </thead>
    );


    return (
        mounted && (
            <Container className="recently-released-wrapper my-5">
                <Header
                    className="header-wrapper"
                    title="Recently Released"
                />
                <NavItems
                    recentlyReleasedUrl={RECENTLY_RELEASED_GAMES}
                    gamesPage={{
                        url: props.url,
                        onInitGamesPage: props.onInitGamesPage
                    }}
                />
                <InfiniteMediaTable
                    page={{
                        url: props.url,
                        items: props.items,
                        page: props.page,
                        hasMoreItems: props.hasMoreItems,
                        onfetchGames: props.onfetchGames,
                        onInitGamesPage: props.onInitGamesPage
                    }}
                    table={{
                        tableHead: tableHead,
                        ItemElement: MediaItem
                    }}
                />
            </Container>
        )
    );
}

const mapStateToProps = state => {
    return {
        url: state.gamesPage.url,
        items: state.gamesPage.items,
        page: state.gamesPage.page,
        hasMoreItems: state.gamesPage.hasMoreItems
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onInitGamesPage: url => dispatch(actions.initGamesPage(url)),
        onfetchGames: (url, page, items) =>
            dispatch(actions.fetchGames(url, page, items))
    };
};

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(RecentlyReleased)
);
