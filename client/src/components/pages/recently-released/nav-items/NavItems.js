import React from "react";
import { Nav, NavDropdown } from "react-bootstrap";
import "./nav-items.css";

export default function NavItems(props) {
  const setUrlWithPlatform = platform => {
    const url = props.recentlyReleasedUrl + `/platform/${platform}`;
    if (url !== props.gamesPage.url) {
      props.gamesPage.onInitGamesPage(url);
    }
  };

  return (
    <Nav className="mb-5" variant="pills" defaultActiveKey="link-0">
      <Nav.Item>
          <Nav.Link
              onClick={() => {
                  setUrlWithPlatform("(6,48,49,130,9,12,37,46,39,34)");
              }}
              eventKey="link-0"
          >All</Nav.Link>
      </Nav.Item>
      <Nav.Item>
        <Nav.Link
          onClick={() => {
            setUrlWithPlatform(6);
          }}
          eventKey="link-1"
        >
          PC
        </Nav.Link>
      </Nav.Item>
      <Nav.Item>
        <Nav.Link
          onClick={() => {
            setUrlWithPlatform(48);
          }}
          eventKey="link-2"
        >
          PlayStation 4
        </Nav.Link>
      </Nav.Item>
      <Nav.Item>
        <Nav.Link
          onClick={() => {
            setUrlWithPlatform(49);
          }}
          eventKey="link-3"
        >
          Xbox One
        </Nav.Link>
      </Nav.Item>
      <Nav.Item>
        <Nav.Link
          onClick={() => {
            setUrlWithPlatform(130);
          }}
          eventKey="link-4"
        >
          Nintendo Switch
        </Nav.Link>
      </Nav.Item>
      <Nav.Item>
        <Nav.Link
          onClick={() => {
            setUrlWithPlatform(9);
          }}
          eventKey="link-5"
        >
          PlayStation 3
        </Nav.Link>
      </Nav.Item>
      <Nav.Item>
        <Nav.Link
          onClick={() => {
            setUrlWithPlatform(12);
          }}
          eventKey="link-6"
        >
          Xbox 360
        </Nav.Link>
      </Nav.Item>
      <Nav.Item>
        <Nav.Link
          onClick={() => {
            setUrlWithPlatform(37);
          }}
          eventKey="link-7"
        >
          Nintendo 3DS
        </Nav.Link>
      </Nav.Item>
      <NavDropdown title="More" id="nav-dropdown">
        <Nav.Item>
          <Nav.Link
            onClick={() => {
              setUrlWithPlatform(46);
            }}
            eventKey="link-8"
          >
            PlayStation Vita
          </Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link
            onClick={() => {
              setUrlWithPlatform(39);
            }}
            eventKey="link-9"
          >
            iOS
          </Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link
            onClick={() => {
              setUrlWithPlatform(34);
            }}
            eventKey="link-19"
          >
            Android
          </Nav.Link>
        </Nav.Item>
      </NavDropdown>
    </Nav>
  );
}
