import React from "react";
import {Button, Media} from "react-bootstrap";
import {Link, withRouter} from "react-router-dom";
import noCover from "../../../../assets/img/no-cover.jpg";
import "./media-item.css";
import * as actions from "../../../../store/actions";
import {connect} from "react-redux";

function MediaItem(props) {
    const {game, date} = props.game;
    const gameId = game && game.id;
    const name = game && game.name;
    const cover = game && game.cover && game.cover.url;
    const coverUrl = cover ? cover : noCover;
    const parsedDate = date && new Date(date * 1000);
    const formattedDate =
        parsedDate.getDate() + "-" + (parsedDate.getMonth() + 1) + "-" + parsedDate.getFullYear();

    const gameToBeAdd = {
        id: gameId,
        title: name,
        coverUrl: coverUrl
    }

    const showAddToListForm = () => {
        if (props.isAuth) {
            props.onSetGameToBeAdd(gameToBeAdd);
            props.onShowModal(4);
        } else {
            props.onShowModal(1);
        }
    }

    const getDetails = () => {
        props.history.push("/games/" + gameId);
    };

    return (
        <tr>
            <td className="item-td d-flex align-items-center justify-content-between">
                <Media className="media-item d-flex align-items-start justify-content-center">
                    <img
                        height="40px"
                        width="40px"
                        className="mr-3"
                        src={coverUrl}
                        alt="Table item"
                        onClick={getDetails}
                    />
                    <Media.Body>
                        <Link to={`/games/${gameId}`} className="text-main">
                            {name}
                        </Link>
                    </Media.Body>
                </Media>
                <div className="side-item-wrapper d-flex align-items-center justify-content-center">
                    <small className="text-muted">{formattedDate}</small>
                    <Button onClick={() => {
                        showAddToListForm()
                    }} variant="warning" className="ml-3">
                        <div className="d-flex align-items-center justify-content-center">
                            <i className="fas fa-plus mr-2"></i>
                            <span>List</span>
                        </div>
                    </Button>
                </div>
            </td>
        </tr>
    );
}

const mapStateToProps = state => {
    return {
        isAuth: state.auth.token !== null
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onShowModal: childId => dispatch(actions.modalShow(childId)),
        onSetGameToBeAdd: game =>
            dispatch(actions.setGameToBeAdd(game))
    };
};

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(MediaItem)
);
