import React, {useEffect} from "react";
import {Redirect, Route, Switch, withRouter} from "react-router-dom";
import {connect} from "react-redux";
import "./App.css";
import Layout from "../components/layout/Layout";
import Home from "../components/pages/home/Home";
import GameDetails from "../components/pages/game-details/GameDetails";
import SearchResults from "../components/pages/search-results/SearchResults";
import RecentlyReleased from "../components/pages/recently-released/RecentlyReleased";
import MostAnticipated from "../components/pages/most-anticipated/MostAnticipated";
import ComingSoon from "../components/pages/coming-soon/ComingSoon";
import Reviews from "../components/pages/reviews/UserReviews";
import Lists from "../components/pages/user-game-lists/UserGameLists";
import UserGameList from "../components/pages/user-game-list/UserGameList";
import LatestNews from "../components/pages/latest-news/LatestNews";
import NotFound from "../components/pages/not-found/NotFound";
import ConnectionErrorPage from "../components/connection-error-page/ConnectionErrorPage";
import * as actions from "../store/actions/index";
import axios from "axios";

function App(props) {

    useEffect(() => {
        props.onAutoSignup();
    }, []);

    axios.interceptors.response.use(response => {
        return response;
    }, error => {
        console.log(error)
        if (!error.response) {
            props.history.push("/connectionError");
        }
        return Promise.reject(error);
    });

    return (
        <div className="App">
            <Layout>
                <Switch>
                    <Route exact path="/" component={Home}/>
                    <Route
                        exact
                        path="/games/recentlyReleased"
                        component={RecentlyReleased}
                    />
                    <Route
                        exact
                        path="/games/mostAnticipated"
                        component={MostAnticipated}
                    />
                    <Route exact path="/games/comingSoon" component={ComingSoon}/>
                    <Route exact path="/games/:gameId" component={GameDetails}/>
                    <Route
                        exact
                        path="/games/search/:gameTitle"
                        component={SearchResults}
                    />
                    <Route exact path="/games/reviews/:gameTitle/:gameId" component={Reviews}/>
                    <Route exact path="/latestNews" component={LatestNews}/>
                    <Route exact path="/news/:gameTitle/:gameId" component={LatestNews}/>
                    <Route exact path="/lists/:username" component={Lists}/>
                    <Route exact path="/lists/:username/:listName/:listId" component={UserGameList}/>
                    <Route exact path="/connectionError" component={ConnectionErrorPage}/>
                    <Route exact path='/pageNotFound' component={NotFound}/>
                    <Redirect from='*' to='/pageNotFound'/>
                </Switch>
            </Layout>
        </div>
    );
}

const mapDispatchToProps = dispatch => {
    return {
        onAutoSignup: () => dispatch(actions.authCheckState())
    };
};

export default withRouter(connect(
    null,
    mapDispatchToProps
)(App));
