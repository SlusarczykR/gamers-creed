export const BASE_CLIENT = "http://localhost:3000/";

export const BASE = "http://localhost:7070/";

export const HOME_HEADER = BASE + "api/games/homeHeader";
export const POPULAR_GAMES = BASE + "api/games/popular";
export const RECOMMENDED_GAMES = BASE + "api/games/recommended";
export const PULSES = BASE + "api/games/pulses";
export const RECENTLY_RELEASED_GAMES = BASE + "api/games/recentlyReleased";
export const COMING_SOON_GAMES = BASE + "api/games/comingSoon";
export const MOST_POPULAR_GAMES = BASE + "api/games/mostPopular";

export const GAME_HEADER = BASE + "api/games/header/";
export const GAME_MEDIA = BASE + "api/games/media/";
export const GAME_PULSES = BASE + "api/games/pulses/";
export const GAME_INFO = BASE + "api/games/info/";
export const GAME_SIMILAR_GAMES = BASE + "api/games/similarGames/";
export const GAME_SEARCH_FOR_TITLE = BASE + "api/games/search";

export const LATEST_NEWS = BASE + "api/news";

export const COMMENTS_WITH_ID = BASE + "comments/";

export const ADD_COMMENT = BASE + "comments/";
export const GAME_LISTS = BASE + "lists";

export const GAMES_FOR_LIST = BASE + "lists/games/";
export const ADD_GAME_TO_LIST = BASE + "addGameToList/";
export const DELETE_GAME_FROM_LIST = BASE + "deleteGameFromList/";
