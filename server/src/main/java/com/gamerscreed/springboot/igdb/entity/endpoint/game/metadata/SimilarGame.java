package com.gamerscreed.springboot.igdb.entity.endpoint.game.metadata;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.gamerscreed.springboot.igdb.entity.endpoint.game.media.Cover;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SimilarGame {

	private int id;

	private Cover cover;

	public SimilarGame() {
		this.id = 0;
		this.cover = new Cover();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Cover getCover() {
		return cover;
	}

	public void setCover(Cover cover) {
		this.cover = cover;
	}

	@Override
	public String toString() {
		return "SimilarGame{" +
				"id=" + id +
				", cover=" + cover +
				'}';
	}
}
