package com.gamerscreed.springboot.igdb.entity.endpoint.game;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.gamerscreed.springboot.igdb.entity.endpoint.game.media.Cover;
import com.gamerscreed.springboot.igdb.entity.endpoint.game.media.Video;
import com.gamerscreed.springboot.igdb.entity.endpoint.game.metadata.InvolvedCompanie;
import com.gamerscreed.springboot.igdb.entity.endpoint.game.metadata.ReleaseDate;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Game {

    private int id;
    private String name;
    private String summary;
    private String url;
    private int aggregated_rating_count;
    private double aggregated_rating;
    private Cover cover;
    private List<ReleaseDate> release_dates;
    private Long first_release_date;
    private double popularity;
    private List<NameEndpoint> player_perspectives;
    private List<NameEndpoint> game_engines;
    private List<NameEndpoint> genres;
    private List<UrlEndpoint> screenshots;
    private List<Video> videos;
    private List<NameEndpoint> platforms;
    private List<Game> similar_games;
    private List<NameEndpoint> themes;
    private List<InvolvedCompanie> involved_companies;
    private List<NameEndpoint> game_modes;
    private List<Game> dlcs;
    private List<Game> expansions;

    public Game() {
        this.id = 0;
        this.name = "";
        this.summary = "";
        this.url = "";
        this.aggregated_rating_count = 0;
        this.aggregated_rating = 0.0;
        this.cover = new Cover();
        this.release_dates = new ArrayList<>();
        this.first_release_date = 0L;
        this.popularity = 0.0;
        this.player_perspectives = new ArrayList<>();
        this.game_engines = new ArrayList<>();
        this.genres = new ArrayList<>();
        this.screenshots = new ArrayList<>();
        this.videos = new ArrayList<>();
        this.platforms = new ArrayList<>();
        this.similar_games = new ArrayList<>();
        this.themes = new ArrayList<>();
        this.involved_companies = new ArrayList<>();
        this.game_modes = new ArrayList<>();
        this.dlcs = new ArrayList<>();
        this.expansions = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getRoundedRaiting() {
        return (int) Math.round(aggregated_rating);
    }

    public double getAggregated_rating() {
        return aggregated_rating;
    }

    public void setAggregated_rating(double aggregated_rating) {
        this.aggregated_rating = aggregated_rating;
    }

    public int getAggregated_rating_count() {
        return aggregated_rating_count;
    }

    public void setAggregated_rating_count(int aggregated_rating_count) {
        this.aggregated_rating_count = aggregated_rating_count;
    }

    public Cover getCover() {
        return cover;
    }

    public void setCover(Cover cover) {
        this.cover = cover;
    }

    public Long getFirst_release_date() {
        return first_release_date;
    }

    public void setFirst_release_date(Long firstReleaseDate) {
        this.first_release_date = firstReleaseDate;
    }

    public String getFormatted_first_release_date() {
        return DateFormat.getDateInstance(DateFormat.SHORT)
                .format((long) this.getFirst_release_date() * 1000);
    }

    public List<ReleaseDate> getRelease_dates() {
        return release_dates;
    }

    public void setRelease_dates(List<ReleaseDate> release_dates) {
        this.release_dates = release_dates;
    }

    public double getPopularity() {
        return popularity;
    }

    public void setPopularity(double popularity) {
        this.popularity = popularity;
    }

    public List<NameEndpoint> getPlayer_perspectives() {
        return player_perspectives;
    }

    public void setPlayer_perspectives(List<NameEndpoint> player_perspectives) {
        this.player_perspectives = player_perspectives;
    }

    public List<NameEndpoint> getGame_engines() {
        return game_engines;
    }

    public void setGame_engines(List<NameEndpoint> game_engines) {
        this.game_engines = game_engines;
    }

    public List<NameEndpoint> getGenres() {
        return genres;
    }

    public void setGenres(List<NameEndpoint> genres) {
        this.genres = genres;
    }

    public List<UrlEndpoint> getScreenshots() {
        return screenshots;
    }

    public void setScreenshots(List<UrlEndpoint> screenshots) {
        this.screenshots = screenshots;
    }

    public List<Video> getVideos() {
        return videos;
    }

    public void setVideos(List<Video> videos) {
        this.videos = videos;
    }

    public List<NameEndpoint> getPlatforms() {
        return platforms;
    }

    public void setPlatforms(List<NameEndpoint> platforms) {
        this.platforms = platforms;
    }

    public List<Game> getSimilar_games() {
        return similar_games;
    }

    public void setSimilar_games(List<Game> similar_games) {
        this.similar_games = similar_games;
    }

    public List<NameEndpoint> getThemes() {
        return themes;
    }

    public void setThemes(List<NameEndpoint> themes) {
        this.themes = themes;
    }

    public List<InvolvedCompanie> getInvolved_companies() {
        return involved_companies;
    }

    public void setInvolved_companies(List<InvolvedCompanie> involved_companies) {
        this.involved_companies = involved_companies;
    }

    public List<NameEndpoint> getGame_modes() {
        return game_modes;
    }

    public void setGame_modes(List<NameEndpoint> game_modes) {
        this.game_modes = game_modes;
    }

    public List<Game> getDlcs() {
        return dlcs;
    }

    public void setDlcs(List<Game> dlcs) {
        this.dlcs = dlcs;
    }

    public List<Game> getExpansions() {
        return expansions;
    }

    public void setExpansions(List<Game> expansions) {
        this.expansions = expansions;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "Game{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", summary='" + summary + '\'' +
                ", url='" + url + '\'' +
                ", aggregated_rating_count=" + aggregated_rating_count +
                ", aggregated_rating=" + aggregated_rating +
                ", cover=" + cover +
                ", release_dates=" + release_dates +
                ", first_release_date=" + first_release_date +
                ", popularity=" + popularity +
                ", player_perspectives=" + player_perspectives +
                ", game_engines=" + game_engines +
                ", genres=" + genres +
                ", screenshots=" + screenshots +
                ", videos=" + videos +
                ", platforms=" + platforms +
                ", similar_games=" + similar_games +
                ", themes=" + themes +
                ", involved_companies=" + involved_companies +
                ", game_modes=" + game_modes +
                ", dlcs=" + dlcs +
                ", expansions=" + expansions +
                '}';
    }
}
