package com.gamerscreed.springboot.igdb.gamelist.game.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "games")
public class Game {

    @EmbeddedId
    private GameId id;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "cover_url", nullable = false)
    private String coverUrl;

    public Game() {
    }

    public Game(String title, String coverUrl) {
        this.title = title;
        this.coverUrl = coverUrl;
    }

    public GameId getId() {
        return id;
    }

    public void setId(GameId id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }

    @Override
    public String toString() {
        return "Game{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", coverUrl='" + coverUrl + '\'' +
                '}';
    }
}
