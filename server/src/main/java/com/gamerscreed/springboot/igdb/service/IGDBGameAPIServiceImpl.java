package com.gamerscreed.springboot.igdb.service;

import com.gamerscreed.springboot.igdb.entity.endpoint.game.Game;
import com.gamerscreed.springboot.igdb.entity.endpoint.game.UrlEndpoint;
import com.gamerscreed.springboot.igdb.entity.endpoint.game.details.GameInfo;
import com.gamerscreed.springboot.igdb.entity.endpoint.game.media.Cover;
import com.gamerscreed.springboot.igdb.entity.endpoint.game.metadata.ReleaseDate;
import com.gamerscreed.springboot.igdb.entity.endpoint.pulse.Pulse;
import com.gamerscreed.springboot.igdb.entity.endpoint.pulse.PulseGroup;
import com.gamerscreed.springboot.igdb.entity.endpoint.response.GameDetailsResponse;
import com.gamerscreed.springboot.igdb.entity.request.IgdbHomeRequestBody;
import com.gamerscreed.springboot.igdb.entity.request.IgdbItemsWithOffsetRequestBody;
import com.gamerscreed.springboot.igdb.fetcher.IgdbDataFetcher;
import com.gamerscreed.springboot.igdb.parser.IgdbDataParser;
import com.gamerscreed.springboot.igdb.utils.DateUtils;
import com.gamerscreed.springboot.igdb.service.exception.MissingFetchedDataException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.net.URISyntaxException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import static com.gamerscreed.springboot.igdb.constant.GameGroups.*;
import static com.gamerscreed.springboot.igdb.constant.GameSections.*;
import static com.gamerscreed.springboot.igdb.constant.ImageSizes.COVER_BIG;
import static com.gamerscreed.springboot.igdb.constant.ImageSizes.SCREENSHOT_HUGE;
import static com.gamerscreed.springboot.igdb.entity.request.IgdbEndpoints.*;
import static com.gamerscreed.springboot.igdb.entity.request.IgdbGameDetailsRequestBody.getBodyWithGivenId;
import static com.gamerscreed.springboot.igdb.entity.request.IgdbGameDetailsRequestBody.getBodyWithGivenPulseGroupId;

@Service
public class IGDBGameAPIServiceImpl implements GameAPIService {

    private static final Logger LOGGER = LoggerFactory.getLogger(IGDBGameAPIServiceImpl.class.getName());

    private static final String GIVEN_PAGE = "givenPage";
    private static final String GIVEN_ID = "givenId";
    private static final String GIVEN_TITLE = "givenTitle";
    private static final int FIRST_ELEM_INDEX = 0;

    private IgdbDataParser dataParser;
    private IgdbDataFetcher dataFetcher;

    @Autowired
    public IGDBGameAPIServiceImpl(IgdbDataParser dataParser, IgdbDataFetcher dataFetcher) {
        this.dataParser = dataParser;
        this.dataFetcher = dataFetcher;
    }

    @Override
    public List<Game> getPopularGames() throws URISyntaxException, ExecutionException, InterruptedException {
        List<Game> popularGamesList = getData(GAMES, IgdbHomeRequestBody.POPULAR_GAMES, Game[].class);
        if (!CollectionUtils.isEmpty(popularGamesList)) {
            setRandomCoversForGames(popularGamesList);
            Collections.shuffle(popularGamesList);
            return dataParser.truncateGamesList(popularGamesList, FIRST_ELEM_INDEX, 6);
        }
        return new ArrayList<>();
    }

    private void setRandomCoversForGames(List<Game> popularGamesList) {
        popularGamesList.stream()
                .filter(Objects::nonNull)
                .forEach(this::setRandomGameCover);
    }

    private void setRandomGameCover(Game game) {
        List<UrlEndpoint> screenshots = game.getScreenshots();
        if (!CollectionUtils.isEmpty(screenshots)) {
            dataParser.changeScreenshotsSize(screenshots, SCREENSHOT_HUGE);
            game.setCover(new Cover(screenshots.get(new Random().nextInt(screenshots.size())).getUrl()));
        } else {
            dataParser.changeGameCoverSize(game, SCREENSHOT_HUGE);
        }
    }

    @Override
    public List<Game> getRecommendedGames() throws URISyntaxException, ExecutionException, InterruptedException {
        List<Game> recommendedGamesList = getData(GAMES, IgdbHomeRequestBody.getBodyWithGivenDate(
                DateUtils.getCurrentTime(), RECOMMENDED_GAMES), Game[].class);
        if (recommendedGamesList != null) {
            changeSizeOfCovers(recommendedGamesList);
            Collections.shuffle(recommendedGamesList);
        }
        return recommendedGamesList;
    }

    private void changeSizeOfCovers(List<Game> recommendedGamesList) {
        recommendedGamesList.stream()
                .filter(Objects::nonNull)
                .forEach(game -> dataParser.changeGameCoverSize(game, COVER_BIG));
    }

    @Override
    public List<ReleaseDate> getReleaseDateByGroup(String gamesGroup) throws InterruptedException, ExecutionException,
            URISyntaxException {
        List<ReleaseDate> recentlyReleasedList = getDistinctReleaseDatesByGames(RELEASE_DATES,
                IgdbHomeRequestBody.getBodyWithGivenDate((DateUtils.getCurrentTime()), gamesGroup));
        if (recentlyReleasedList != null) {
            return dataParser.truncateGamesList(recentlyReleasedList, FIRST_ELEM_INDEX, 10);
        }
        return new ArrayList<>();
    }

    @Override
    public List<Game> getGamesByGroup(String gamesGroup) throws URISyntaxException, ExecutionException,
            InterruptedException {
        List<Game> mostPopularGamesList = getData(GAMES,
                IgdbHomeRequestBody.getBodyWithGivenDate((DateUtils.getCurrentTime()), gamesGroup), Game[].class);
        if (mostPopularGamesList != null) {
            return dataParser.truncateGamesList(mostPopularGamesList, FIRST_ELEM_INDEX, 10);
        }
        return new ArrayList<>();
    }

    @Override
    public GameDetailsResponse getGameHeader(int id) throws URISyntaxException, ExecutionException, InterruptedException, MissingFetchedDataException {
        Game targetGame = getSingleData(GAMES, getBodyWithGivenId(id,
                GAME_HEADER), Game[].class).orElseThrow(MissingFetchedDataException::new);
        if (targetGame != null) {
            String headerUrl = "";
            dataParser.changeGameCoverSize(targetGame, COVER_BIG);
            List<UrlEndpoint> screenshots = targetGame.getScreenshots();
            if (!screenshots.isEmpty()) {
                dataParser.changeScreenshotsSize(screenshots, SCREENSHOT_HUGE);
                headerUrl = screenshots.get(new Random().nextInt(screenshots.size())).getUrl();
            }
            return new GameDetailsResponse(targetGame, headerUrl, targetGame.getFormatted_first_release_date());
        }
        return new GameDetailsResponse();
    }

    @Override
    public Game getGameMedia(int id) throws URISyntaxException, ExecutionException, InterruptedException, MissingFetchedDataException {
        Game targetGame = getSingleData(GAMES, getBodyWithGivenId(id,
                GAME_MEDIA), Game[].class).orElseThrow(MissingFetchedDataException::new);
        if (targetGame != null) {
            dataParser.changeScreenshotsSize(targetGame.getScreenshots(), SCREENSHOT_HUGE);
        }
        return targetGame;
    }

    @Override
    public List<Game> getGameSimilarGames(int id) throws URISyntaxException, ExecutionException, InterruptedException, MissingFetchedDataException {
        Game targetGame = getSingleData(GAMES, getBodyWithGivenId(id,
                GAME_SIMILAR_GAMES), Game[].class).orElseThrow(MissingFetchedDataException::new);

        List<Game> similarGames = (targetGame != null) ? targetGame.getSimilar_games() : new ArrayList<>();
        if (!similarGames.isEmpty()) {
            dataParser.changeSimilarGamesCoverSize(similarGames, COVER_BIG);
            Collections.shuffle(similarGames);
        }
        return similarGames;
    }

    @Override
    public GameInfo getGameInfoSection(int id) throws URISyntaxException, ExecutionException, InterruptedException, MissingFetchedDataException {
        return getSingleData(GAMES, getBodyWithGivenId(id, GAME_INFO), GameInfo[].class).orElseThrow(MissingFetchedDataException::new);
    }

    @Override
    public List<Pulse> getHomePulses() throws URISyntaxException, ExecutionException, InterruptedException {
        return getData(PULSES, IgdbHomeRequestBody.PULSES, Pulse[].class);
    }

    @Override
    public List<Pulse> getGamePulses(int id) throws InterruptedException, ExecutionException, URISyntaxException {
        List<PulseGroup> gamePulseGroup = getData(PULSE_GROUPS, getBodyWithGivenId(id, GAME_PULSES), PulseGroup[].class);
        if (gamePulseGroup != null) {
            return getMappedPulses(gamePulseGroup);
        }
        return new ArrayList<>();
    }

    @Override
    public List<Pulse> getGamePulses(int id, int page) throws URISyntaxException, ExecutionException, InterruptedException {
        Map<String, String> params = new HashMap<>();
        params.put(GIVEN_ID, String.valueOf(id));
        params.put(GIVEN_PAGE, String.valueOf(page));

        List<PulseGroup> gamePulseGroup = getData(PULSE_GROUPS,
                getBodyWithGivenPulseGroupId(params), PulseGroup[].class);

        if (gamePulseGroup != null) {
            return getMappedPulses(gamePulseGroup);
        }
        return new ArrayList<>();
    }

    private List<Pulse> getMappedPulses(List<PulseGroup> targetGamePulseGroup) {
        return targetGamePulseGroup.stream()
                .filter(Objects::nonNull)
                .map(PulseGroup::getPulses)
                .map(pulseGroup -> pulseGroup.get(FIRST_ELEM_INDEX))
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    @Override
    public List<Game> getSearchGameResults(String title, int page) throws URISyntaxException, ExecutionException, InterruptedException {
        Map<String, String> params = new HashMap<>();
        params.put(GIVEN_PAGE, String.valueOf(page));
        params.put(GIVEN_TITLE, String.valueOf(title));

        return getData(GAMES, IgdbItemsWithOffsetRequestBody.getBodyWithGivenPage(GAMES_WITH_TITLE, params), Game[].class);
    }

    @Override
    public List<Pulse> getLatestPulses(int page) throws URISyntaxException, ExecutionException, InterruptedException {
        Map<String, String> params = new HashMap<>();
        params.put(GIVEN_PAGE, String.valueOf(page));

        return getData(PULSES, IgdbItemsWithOffsetRequestBody.getBodyWithGivenPage(LATEST_PULSES, params), Pulse[].class);
    }


    @Override
    public List<Game> getMostPopularGames(Map<String, String> params) throws URISyntaxException, ExecutionException,
            InterruptedException {
        return getData(GAMES, IgdbItemsWithOffsetRequestBody.getBodyWithGivenPage(MOST_ANTICIPATED_GAMES, params),
                Game[].class);
    }

    @Override
    public List<ReleaseDate> getRecentlyReleasedGames(Map<String, String> params) throws InterruptedException,
            ExecutionException, URISyntaxException {
        return getDistinctReleaseDatesByGames(RELEASE_DATES,
                IgdbItemsWithOffsetRequestBody.getBodyWithGivenPage(RECENTLY_RELEASED_GAMES, params));
    }

    @Override
    public List<Game> getComingSoonGames(Map<String, String> params) throws URISyntaxException, ExecutionException,
            InterruptedException {
        return getData(GAMES,
                IgdbItemsWithOffsetRequestBody.getBodyWithGivenPeriodAndPage(COMING_SOON_GAMES, params), Game[].class);
    }

    private <T> Optional<T> getSingleData(String url, String body, Class<T[]> type) throws URISyntaxException, ExecutionException,
            InterruptedException {
        CompletableFuture<List<T>> dataList = dataFetcher.fetchData(url, body, type);
        List<T> data = dataList.get();
        return (!data.isEmpty()) ? Optional.ofNullable(data.get(FIRST_ELEM_INDEX)) : Optional.empty();
    }

    private <T> List<T> getData(String url, String body, Class<T[]> type) throws URISyntaxException, ExecutionException,
            InterruptedException {
        CompletableFuture<List<T>> dataList = dataFetcher.fetchData(url, body, type);
        List<T> data = dataList.get();
        return (!data.isEmpty()) ? data.stream().filter(Objects::nonNull).collect(Collectors.toList()) : new ArrayList<>();
    }

    private List<ReleaseDate> getDistinctReleaseDatesByGames(String url, String body) throws InterruptedException,
            ExecutionException, URISyntaxException {
        List<ReleaseDate> releaseDates = getData(url, body, ReleaseDate[].class);
        return (releaseDates != null) ? dataParser.distinctGamesByTitle(releaseDates) : new ArrayList<>();
    }
}
