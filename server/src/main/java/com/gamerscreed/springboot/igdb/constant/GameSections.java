package com.gamerscreed.springboot.igdb.constant;

public class GameSections {

    public static final String GAME_HEADER = "GAME_HEADER";
    public static final String GAME_MEDIA = "GAME_MEDIA";
    public static final String GAME_INFO = "GAME_INFO";
    public static final String GAME_SIMILAR_GAMES = "GAME_SIMILAR_GAMES";
    public static final String GAME_PULSES = "GAME_PULSES";
}
