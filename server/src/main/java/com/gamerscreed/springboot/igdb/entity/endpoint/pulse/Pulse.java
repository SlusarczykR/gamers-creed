package com.gamerscreed.springboot.igdb.entity.endpoint.pulse;

import java.text.DateFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.gamerscreed.springboot.igdb.entity.endpoint.game.UrlEndpoint;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Pulse {

	private int id;
	private String author;
	private String title;
	private String summary;
	private Long created_at;
	private String image;
	private UrlEndpoint website;
	private PulseSource pulse_source;

	public Pulse() {
		this.id = 0;
		this.author = "";
		this.title = "";
		this.summary = "";
		this.created_at = 0L;
		this.image = "";
		this.website = null;
		this.pulse_source = null;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public Long getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Long created_at) {
		this.created_at = created_at;
	}

	public static String parseStringCreated_at(Long created_at) {
		return DateFormat.getDateInstance(DateFormat.SHORT).format(created_at * 1000);
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public UrlEndpoint getWebsite() {
		return website;
	}

	public void setWebsite(UrlEndpoint website) {
		this.website = website;
	}

	public PulseSource getPulse_source() {
		return pulse_source;
	}

	public void setPulse_source(PulseSource pulse_source) {
		this.pulse_source = pulse_source;
	}

	@Override
	public String toString() {
		return "Pulse [id=" + id + ", author=" + author + ", title=" + title + ", summary=" + summary + ", created_at="
				+ created_at + ", image=" + image + ", website=" + website + ", pulse_source=" + pulse_source + "]";
	}

}
