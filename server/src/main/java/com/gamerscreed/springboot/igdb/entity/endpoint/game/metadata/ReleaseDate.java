package com.gamerscreed.springboot.igdb.entity.endpoint.game.metadata;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.gamerscreed.springboot.igdb.entity.endpoint.game.Game;
import com.gamerscreed.springboot.igdb.entity.endpoint.game.NameEndpoint;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ReleaseDate {

    private Game game;
    private Long date;
    private NameEndpoint platform;

    public ReleaseDate() {
        this.game = null;
        this.date = 0L;
        this.platform = null;
    }

    public ReleaseDate(Game game, Long date, NameEndpoint platform) {
        this.game = game;
        this.date = date;
        this.platform = platform;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public NameEndpoint getPlatform() {
        return platform;
    }

    public void setPlatform(NameEndpoint platform) {
        this.platform = platform;
    }

    @Override
    public String toString() {
        return "ReleaseDate [game=" + game + ", date=" + date + ", platform=" + platform + "]";
    }

}
