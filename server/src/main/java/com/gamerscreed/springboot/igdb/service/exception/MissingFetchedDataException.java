package com.gamerscreed.springboot.igdb.service.exception;

public class MissingFetchedDataException extends Exception {

    public MissingFetchedDataException() {
    }

    public MissingFetchedDataException(String message) {
        super(message);
    }
}
