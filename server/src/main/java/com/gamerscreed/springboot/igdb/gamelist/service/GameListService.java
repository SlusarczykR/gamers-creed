package com.gamerscreed.springboot.igdb.gamelist.service;

import com.gamerscreed.springboot.igdb.gamelist.game.entity.Game;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface GameListService {

    List<GameListServiceImpl.GameListResponse> getGameListByUser(String username);

    ResponseEntity<String> createGameList(String username, String listName);

    ResponseEntity<String> deleteGameList(String username, long listId);

    List<Game> getGameList(long listId);

    ResponseEntity<String> addGameToGameList(String username, long listId, long gameId, Game game);

    ResponseEntity<String> deleteGameFromGameList(String username, long listId, long gameId);
}
