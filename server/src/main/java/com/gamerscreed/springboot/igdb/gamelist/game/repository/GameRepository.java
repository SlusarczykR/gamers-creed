package com.gamerscreed.springboot.igdb.gamelist.game.repository;

import com.gamerscreed.springboot.igdb.gamelist.game.entity.Game;
import com.gamerscreed.springboot.igdb.gamelist.game.entity.GameId;
import com.gamerscreed.springboot.igdb.gamelist.entity.GameList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface GameRepository extends JpaRepository<Game, GameId> {

    List<Game> findAllByIdGameList(GameList gameList);

    Optional<Game> findByIdGameIdAndIdGameList(long gameId, GameList gameList);

    long countAllByIdGameList(GameList gameList);
}
