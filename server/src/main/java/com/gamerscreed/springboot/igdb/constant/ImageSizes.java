package com.gamerscreed.springboot.igdb.constant;

public class ImageSizes {

    public static final String SCREENSHOT_HUGE = "screenshot_huge";
    public static final String COVER_BIG = "cover_big";
    public static final String THUMB = "thumb";
}
