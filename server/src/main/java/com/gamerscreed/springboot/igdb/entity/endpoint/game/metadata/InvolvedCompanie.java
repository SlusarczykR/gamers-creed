package com.gamerscreed.springboot.igdb.entity.endpoint.game.metadata;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.gamerscreed.springboot.igdb.entity.endpoint.game.NameEndpoint;

@JsonIgnoreProperties(ignoreUnknown = true)
public class InvolvedCompanie {

	private NameEndpoint company;

	public InvolvedCompanie() {
		this.company = null;
	}

	public InvolvedCompanie(NameEndpoint company) {
		this.company = company;
	}

	public NameEndpoint getCompany() {
		return company;
	}

	public void setCompany(NameEndpoint company) {
		this.company = company;
	}

	@Override
	public String toString() {
		return "InvolvedCompanie [company=" + company + "]";
	}

}
