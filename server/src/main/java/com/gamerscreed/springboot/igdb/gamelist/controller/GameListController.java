package com.gamerscreed.springboot.igdb.gamelist.controller;

import com.gamerscreed.springboot.igdb.gamelist.game.entity.Game;
import com.gamerscreed.springboot.igdb.gamelist.service.GameListService;
import com.gamerscreed.springboot.igdb.gamelist.service.GameListServiceImpl;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin
public class GameListController {

    private static final Logger LOGGER = LoggerFactory.getLogger(GameListController.class.getName());
    private static final String INVALID_PARAMS_MESSAGE = "Invalid parameters";

    private GameListService gameListService;

    @Autowired
    public GameListController(GameListService gameListService) {
        this.gameListService = gameListService;
    }

    @GetMapping("lists")
    public List<GameListServiceImpl.GameListResponse> getGameListByUsername(Authentication authentication) {
        return gameListService.getGameListByUser(authentication.getName());
    }

    @PostMapping("lists/{listName}")
    public ResponseEntity<String> createGameListForUser(Authentication authentication, @PathVariable String listName) {
        String username = authentication.getName();
        if (StringUtils.isNotBlank(listName)) {
            return gameListService.createGameList(username, listName);
        }
        return new ResponseEntity<>(INVALID_PARAMS_MESSAGE, HttpStatus.NO_CONTENT);
    }

    @DeleteMapping("lists/{listId}")
    public ResponseEntity<String> deleteGameListWithId(Authentication authentication, @PathVariable long listId) {
        String username = authentication.getName();
        if (listId != 0) {
            return gameListService.deleteGameList(username, listId);
        }
        return new ResponseEntity<>(INVALID_PARAMS_MESSAGE, HttpStatus.NO_CONTENT);
    }

    @GetMapping("lists/games/{listId}")
    public List<Game> getGameListById(@PathVariable long listId) {
        if (listId != 0) {
            return gameListService.getGameList(listId);
        }
        return new ArrayList<>();
    }

    @PostMapping("addGameToList/{listId}/{gameId}")
    public ResponseEntity<String> addGameToGameListForUser(Authentication authentication, @PathVariable long listId,
                                                           @PathVariable long gameId, @RequestBody Game game) {
        if (areAddGameToGameListParamsValid(listId, gameId, game)) {
            return gameListService.addGameToGameList(authentication.getName(), listId, gameId, game);
        }
        return new ResponseEntity<>(INVALID_PARAMS_MESSAGE, HttpStatus.NO_CONTENT);
    }

    private boolean areAddGameToGameListParamsValid(long listId, long gameId, Game game) {
        return (listId != 0) && (gameId != 0) && (game != null);
    }

    @DeleteMapping("deleteGameFromList/{listId}/{gameId}")
    public ResponseEntity<String> deleteGameFromList(Authentication authentication, @PathVariable long listId,
                                                     @PathVariable long gameId) {
        if (areDeleteGameFromListParamsValid(listId, gameId)) {
            return gameListService.deleteGameFromGameList(authentication.getName(), listId, gameId);
        }
        return new ResponseEntity<>(INVALID_PARAMS_MESSAGE, HttpStatus.NO_CONTENT);
    }

    private boolean areDeleteGameFromListParamsValid(long listId, long gameId) {
        return (listId != 0) && (gameId != 0);
    }
}
