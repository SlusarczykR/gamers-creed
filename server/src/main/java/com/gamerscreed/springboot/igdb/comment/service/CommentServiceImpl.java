package com.gamerscreed.springboot.igdb.comment.service;

import com.gamerscreed.springboot.igdb.comment.entity.Comment;
import com.gamerscreed.springboot.igdb.comment.entity.CommentId;
import com.gamerscreed.springboot.igdb.comment.repository.CommentReposoitory;
import com.gamerscreed.springboot.springsecurity.entity.User;
import com.gamerscreed.springboot.springsecurity.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CommentServiceImpl implements CommentService {

    private static final String SUCCESSFUL_CREATION_MESSAGE = "Comment has been successfully created";
    private static final String INVALID_PARAMS_MESSAGE = "Invalid parameters";

    private CommentReposoitory commentReposoitory;
    private UserRepository userRepository;

    @Autowired
    public CommentServiceImpl(CommentReposoitory commentReposoitory, UserRepository userRepository) {
        this.commentReposoitory = commentReposoitory;
        this.userRepository = userRepository;
    }

    @Override
    public List<Comment> getAllComments(int gameId) {
        return commentReposoitory.findByIdGameId(gameId);
    }

    @Override
    public ResponseEntity<String> addComment(String username, int gameId, String body) {
        Optional<User> user = userRepository.findById(username);
        return user.map(value -> createCommentAndReturnMessage(gameId, body, value))
                .orElseGet(() -> new ResponseEntity<>(INVALID_PARAMS_MESSAGE, HttpStatus.NOT_ACCEPTABLE));
    }

    private ResponseEntity<String> createCommentAndReturnMessage(int gameId, String body, User user) {
        Comment newComment = new Comment(new CommentId(user, gameId), body);
        commentReposoitory.save(newComment);
        return new ResponseEntity<>(SUCCESSFUL_CREATION_MESSAGE, HttpStatus.OK);
    }

}
