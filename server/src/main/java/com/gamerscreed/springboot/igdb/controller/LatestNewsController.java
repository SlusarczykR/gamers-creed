package com.gamerscreed.springboot.igdb.controller;

import com.gamerscreed.springboot.igdb.entity.endpoint.pulse.Pulse;
import com.gamerscreed.springboot.igdb.service.GameAPIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.logging.Logger;

@RestController
@CrossOrigin
@RequestMapping("/api/*")
public class LatestNewsController {

    private static final Logger LOGGER = Logger.getLogger(LatestNewsController.class.getName());

    private GameAPIService gameAPIService;

    @Autowired
    public LatestNewsController(GameAPIService gameAPIService) {
        this.gameAPIService = gameAPIService;
    }

    @GetMapping("news/{page}")
    public List<Pulse> getLatestNews(@PathVariable("page") int page)
            throws InterruptedException, ExecutionException, URISyntaxException {
        return gameAPIService.getLatestPulses(page);
    }
}
