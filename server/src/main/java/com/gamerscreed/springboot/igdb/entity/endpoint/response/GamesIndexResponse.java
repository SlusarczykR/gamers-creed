package com.gamerscreed.springboot.igdb.entity.endpoint.response;

import com.gamerscreed.springboot.igdb.entity.endpoint.game.Game;
import com.gamerscreed.springboot.igdb.entity.endpoint.game.metadata.ReleaseDate;
import com.gamerscreed.springboot.igdb.entity.endpoint.pulse.Pulse;

import java.util.List;

public class GamesIndexResponse {

    List<Game> popularGames;
    List<Game> recommendedGames;
    List<ReleaseDate> recentlyReleasedGames;
    List<ReleaseDate> comingSoonGames;
    List<Game> mostPopularGames;
    List<Pulse> recentPulses;

    public GamesIndexResponse(List<Game> popularGames, List<Game> recommendedGames, List<ReleaseDate> recentlyReleasedGames, List<ReleaseDate> comingSoonGames, List<Game> mostPopularGames, List<Pulse> recentPulses) {
        this.popularGames = popularGames;
        this.recommendedGames = recommendedGames;
        this.recentlyReleasedGames = recentlyReleasedGames;
        this.comingSoonGames = comingSoonGames;
        this.mostPopularGames = mostPopularGames;
        this.recentPulses = recentPulses;
    }

    public List<Game> getPopularGames() {
        return popularGames;
    }

    public void setPopularGames(List<Game> popularGames) {
        this.popularGames = popularGames;
    }

    public List<Game> getRecommendedGames() {
        return recommendedGames;
    }

    public void setRecommendedGames(List<Game> recommendedGames) {
        this.recommendedGames = recommendedGames;
    }

    public List<ReleaseDate> getRecentlyReleasedGames() {
        return recentlyReleasedGames;
    }

    public void setRecentlyReleasedGames(List<ReleaseDate> recentlyReleasedGames) {
        this.recentlyReleasedGames = recentlyReleasedGames;
    }

    public List<ReleaseDate> getComingSoonGames() {
        return comingSoonGames;
    }

    public void setComingSoonGames(List<ReleaseDate> comingSoonGames) {
        this.comingSoonGames = comingSoonGames;
    }

    public List<Game> getMostPopularGames() {
        return mostPopularGames;
    }

    public void setMostPopularGames(List<Game> mostPopularGames) {
        this.mostPopularGames = mostPopularGames;
    }

    public List<Pulse> getRecentPulses() {
        return recentPulses;
    }

    public void setRecentPulses(List<Pulse> recentPulses) {
        this.recentPulses = recentPulses;
    }

    @Override
    public String toString() {
        return "GamesIndexResponse{" +
                "popularGames=" + popularGames +
                ", recommendedGames=" + recommendedGames +
                ", recentlyReleasedGames=" + recentlyReleasedGames +
                ", comingSoonGames=" + comingSoonGames +
                ", mostPopularGames=" + mostPopularGames +
                ", recentPulses=" + recentPulses +
                '}';
    }
}
