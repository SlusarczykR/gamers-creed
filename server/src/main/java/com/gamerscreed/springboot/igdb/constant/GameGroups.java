package com.gamerscreed.springboot.igdb.constant;

public class GameGroups {

    public static final String RECENTLY_RELEASED_GAMES = "RECENTLY_RELEASED_GAMES";
    public static final String MOST_ANTICIPATED_GAMES = "MOST_ANTICIPATED";
    public static final String COMING_SOON_GAMES = "COMING_SOON_GAMES";
    public static final String MOST_POPULAR_GAMES = "MOST_POPULAR_GAMES";
    public static final String RECOMMENDED_GAMES = "RECOMMENDED_GAMES";
    public static final String GAMES_WITH_TITLE = "GAMES_WITH_TITLE";
    public static final String LATEST_PULSES = "LATEST_PULSES";
}
