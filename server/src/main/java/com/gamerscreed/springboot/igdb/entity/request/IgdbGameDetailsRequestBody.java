package com.gamerscreed.springboot.igdb.entity.request;

import java.util.Map;

import static com.gamerscreed.springboot.igdb.entity.request.helper.ParamsHelper.replaceGivenPage;
import static com.gamerscreed.springboot.igdb.entity.request.helper.ParamsHelper.replaceParams;

public class IgdbGameDetailsRequestBody {

    private static final String GAME_HEADER = "fields name, summary, aggregated_rating, aggregated_rating_count, first_release_date, cover.url, url, screenshots.url, genres.name, "
            + "popularity, platforms.name, dlcs.name, expansions.name; where id = givenId;";
    private static final String GAME_MEDIA = "fields screenshots.url, videos.video_id; where id = givenId;";
    private static final String GAME_INFO = "fields name, first_release_date, platforms.name, genres.name, player_perspectives.name, game_engines.name, "
            + "themes.name, involved_companies.company.name, game_modes.name, dlcs.name, expansions.name; where id = givenId;";
    private static final String GAME_PULSES = "fields pulses.pulse_source.page.name, pulses.pulse_source.page.url, pulses.author, pulses.created_at, pulses.title, "
            + "pulses.summary, pulses.image, pulses.website.url, game; where game = givenId; sort created_at desc; limit 3;";
    private static final String All_GAME_PULSES = "fields pulses.pulse_source.page.name, pulses.pulse_source.page.url, pulses.author, pulses.created_at, pulses.title, "
            + "pulses.summary, pulses.image, pulses.website.url, game; where game = givenId; sort created_at desc; limit 30; offset givenPage;";
    private static final String GAME_SIMILAR_GAMES = "fields similar_games.id, similar_games.name, similar_games.cover.url; where id = givenId;";
    private static final String GIVEN_ID = "givenId";

    public static String getBodyWithGivenId(int id, String field) {
        switch (field) {
            case "GAME_HEADER":
                return GAME_HEADER.replace(GIVEN_ID, String.valueOf(id));
            case "GAME_MEDIA":
                return GAME_MEDIA.replace(GIVEN_ID, String.valueOf(id));
            case "GAME_INFO":
                return GAME_INFO.replace(GIVEN_ID, String.valueOf(id));
            case "GAME_SIMILAR_GAMES":
                return GAME_SIMILAR_GAMES.replace(GIVEN_ID, String.valueOf(id));
            case "GAME_PULSES":
                return GAME_PULSES.replace(GIVEN_ID, String.valueOf(id));
            default:
                return "";
        }
    }

    public static String getBodyWithGivenPulseGroupId(Map<String, String> replacements) {
        replaceGivenPage(replacements);
        return replaceParams(All_GAME_PULSES, replacements);
    }
}
