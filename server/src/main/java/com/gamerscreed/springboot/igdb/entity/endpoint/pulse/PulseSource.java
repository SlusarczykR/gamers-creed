package com.gamerscreed.springboot.igdb.entity.endpoint.pulse;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.gamerscreed.springboot.igdb.entity.endpoint.game.metadata.Page;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PulseSource {

	private Page page;

	public PulseSource() {
		this.page = null;
	}

	public PulseSource(Page page) {
		this.page = page;
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	@Override
	public String toString() {
		return "PulseSource [page=" + page + "]";
	}

}
