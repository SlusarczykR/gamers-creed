package com.gamerscreed.springboot.igdb.controller;

import com.gamerscreed.springboot.igdb.entity.endpoint.game.Game;
import com.gamerscreed.springboot.igdb.entity.endpoint.game.metadata.ReleaseDate;
import com.gamerscreed.springboot.igdb.entity.endpoint.pulse.Pulse;
import com.gamerscreed.springboot.igdb.service.GameAPIService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URISyntaxException;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static com.gamerscreed.springboot.igdb.constant.GameGroups.*;

@RestController
@CrossOrigin
@RequestMapping("/api/*")
public class HomeController {

    private static final Logger LOGGER = LoggerFactory.getLogger(HomeController.class.getName());

    private GameAPIService gameAPIService;

    @Autowired
    public HomeController(GameAPIService gameAPIService) {
        this.gameAPIService = gameAPIService;
    }

    @GetMapping("games/popular")
    public List<Game> popularGames() throws InterruptedException, URISyntaxException, ExecutionException {
        return gameAPIService.getPopularGames();
    }

    @GetMapping("games/recommended")
    public List<Game> recommendedGames() throws InterruptedException, URISyntaxException, ExecutionException {
        return gameAPIService.getRecommendedGames();
    }

    @GetMapping("games/pulses")
    public List<Pulse> recentGamesPulses() throws InterruptedException, URISyntaxException, ExecutionException {
        return gameAPIService.getHomePulses();
    }

    @GetMapping("games/recentlyReleased")
    public List<ReleaseDate> recentlyReleased() throws InterruptedException, URISyntaxException, ExecutionException {
        return gameAPIService.getReleaseDateByGroup(RECENTLY_RELEASED_GAMES);
    }

    @GetMapping("games/mostPopular")
    public List<Game> mostPopularGames() throws InterruptedException, URISyntaxException, ExecutionException {
        return gameAPIService.getGamesByGroup(MOST_POPULAR_GAMES);
    }

    @GetMapping("games/comingSoon")
    public List<ReleaseDate> comingSoonGames() throws InterruptedException, URISyntaxException, ExecutionException {
        return gameAPIService.getReleaseDateByGroup(COMING_SOON_GAMES);
    }
}
