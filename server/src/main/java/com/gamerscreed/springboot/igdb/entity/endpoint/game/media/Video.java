package com.gamerscreed.springboot.igdb.entity.endpoint.game.media;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Video {

	private String video_id;

	public Video() {
		this.video_id = "";
	}

	public String getVideo_id() {
		return video_id;
	}

	public void setVideo_id(String video_id) {
		this.video_id = video_id;
	}

	@Override
	public String toString() {
		return "Video [video_id=" + video_id + "]";
	}

}
