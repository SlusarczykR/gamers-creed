package com.gamerscreed.springboot.igdb.comment.entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.gamerscreed.springboot.springsecurity.entity.User;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class CommentId implements Serializable {

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH,
            CascadeType.REFRESH})
    @JoinColumn(name = "username")
    @JsonSerialize
    private User user;

    @Column(name = "game_id", nullable = false)
    @JsonSerialize
    private int gameId;

    public CommentId() {
    }

    public CommentId(User user, int gameId) {
        this.user = user;
        this.gameId = gameId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getGameId() {
        return gameId;
    }

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CommentId)) return false;
        CommentId commentId = (CommentId) o;
        return gameId == commentId.gameId &&
                user.equals(commentId.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(user, gameId);
    }

    @Override
    public String toString() {
        return "CommentId{" +
                "user=" + user +
                ", gameId=" + gameId +
                '}';
    }
}
