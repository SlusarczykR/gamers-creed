package com.gamerscreed.springboot.igdb.controller;

import com.gamerscreed.springboot.igdb.entity.endpoint.game.Game;
import com.gamerscreed.springboot.igdb.service.GameAPIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.logging.Logger;

@RestController
@CrossOrigin
@RequestMapping("/api/*")
public class SearchResultsController {

    private static final Logger LOGGER = Logger.getLogger(SearchResultsController.class.getName());

    private GameAPIService gameAPIService;

    @Autowired
    public SearchResultsController(GameAPIService gameAPIService) {
        this.gameAPIService = gameAPIService;
    }

    @GetMapping("games/search/{title}/{page}")
    public List<Game> searchGames(@PathVariable String title, @PathVariable("page") int page)
            throws InterruptedException, ExecutionException, URISyntaxException {
        return gameAPIService.getSearchGameResults(title, page);
    }
}
