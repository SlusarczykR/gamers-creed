package com.gamerscreed.springboot.igdb.gamelist.service;

import com.gamerscreed.springboot.igdb.gamelist.entity.GameList;
import com.gamerscreed.springboot.igdb.gamelist.game.entity.Game;
import com.gamerscreed.springboot.igdb.gamelist.game.entity.GameId;
import com.gamerscreed.springboot.igdb.gamelist.game.repository.GameRepository;
import com.gamerscreed.springboot.igdb.gamelist.repository.GameListRepository;
import com.gamerscreed.springboot.springsecurity.entity.User;
import com.gamerscreed.springboot.springsecurity.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class GameListServiceImpl implements GameListService {

    private static final String FAILED_AUTHORIZATION_MESSAGE = "Your authorization failed";
    private static final String GAME_LIST_NOT_EXISTS_MESSAGE = "Game's list with given name doesn't exist";

    private GameListRepository gameListRepository;
    private GameRepository gameRepository;
    private UserRepository userRepository;

    @Autowired
    public GameListServiceImpl(GameListRepository gameListRepository, GameRepository gameRepository,
                               UserRepository userRepository) {
        this.gameListRepository = gameListRepository;
        this.gameRepository = gameRepository;
        this.userRepository = userRepository;
    }

    @Override
    public List<GameListResponse> getGameListByUser(String username) {
        Optional<User> user = userRepository.findById(username);
        return user
                .map(usr -> gameListRepository.findAllByUser(usr)
                        .stream()
                        .map(list -> new GameListResponse(list, gameRepository.countAllByIdGameList(list)))
                        .collect(Collectors.toList())).orElseGet(ArrayList::new);
    }


    @Override
    public ResponseEntity<String> createGameList(String username, String listName) {
        Optional<User> user = userRepository.findById(username);
        if (user.isPresent()) {
            return proceedGameListCreation(listName, user.get());
        }
        return new ResponseEntity<>("Invalid parameters", HttpStatus.NO_CONTENT);
    }

    private ResponseEntity<String> proceedGameListCreation(String listName, User user) {
        List<GameList> userGameLists = gameListRepository.findAllByUser(user);
        if (isUserAlreadyHasGameList(listName, userGameLists)) {
            return new ResponseEntity<>("Game's list with given name already exists", HttpStatus.NOT_ACCEPTABLE);
        }
        return createGameListAndReturnMessage(listName, user);
    }

    private ResponseEntity<String> createGameListAndReturnMessage(String listName, User user) {
        gameListRepository.save(new GameList(listName, user));
        return new ResponseEntity<>("Game's list has been successfully created", HttpStatus.OK);
    }

    private boolean isUserAlreadyHasGameList(String listName, List<GameList> userGameLists) {
        return userGameLists.stream().map(GameList::getName).anyMatch(name -> name.equals(listName));
    }

    @Override
    public ResponseEntity<String> deleteGameList(String username, long listId) {
        Optional<GameList> fetchedGameList = gameListRepository.findById(listId);
        if (fetchedGameList.isPresent()) {
            GameList gameList = fetchedGameList.get();
            return checkUserAndProceedGameListDeletion(username, gameList);
        }
        return new ResponseEntity<>("Game's list with given name doesn't exist in database",
                HttpStatus.NOT_ACCEPTABLE);
    }

    private ResponseEntity<String> checkUserAndProceedGameListDeletion(String username, GameList gameList) {
        if (isGameListAssignToUser(username, gameList)) {
            return deleteGameListAndReturnMessage(gameList);
        }
        return new ResponseEntity<>(FAILED_AUTHORIZATION_MESSAGE, HttpStatus.UNAUTHORIZED);
    }

    private ResponseEntity<String> deleteGameListAndReturnMessage(GameList gameList) {
        gameListRepository.delete(gameList);
        return new ResponseEntity<>("Game's list has been successfully deleted", HttpStatus.OK);
    }

    @Override
    public List<Game> getGameList(long listId) {
        Optional<GameList> listInDB = gameListRepository.findById(listId);
        if (listInDB.isPresent()) {
            return gameRepository.findAllByIdGameList(listInDB.get());
        }
        return new ArrayList<>();
    }

    @Override
    public ResponseEntity<String> addGameToGameList(String username, long listId, long gameId, Game game) {
        Optional<GameList> listInDB = gameListRepository.findById(listId);
        if (!listInDB.isPresent()) {
            return new ResponseEntity<>(GAME_LIST_NOT_EXISTS_MESSAGE, HttpStatus.NOT_ACCEPTABLE);
        }
        GameList gameList = listInDB.get();
        return checkUserAndProceedGameAddition(username, gameId, game, gameList);
    }

    private ResponseEntity<String> checkUserAndProceedGameAddition(String username, long gameId, Game game, GameList gameList) {
        if (isGameListAssignToUser(username, gameList)) {
            return proceedAddGameToGameList(gameId, game, gameList);
        }
        return new ResponseEntity<>(FAILED_AUTHORIZATION_MESSAGE, HttpStatus.UNAUTHORIZED);
    }

    private boolean isGameListAssignToUser(String username, GameList gameList) {
        return username.equals(gameList.getUser().getUsername());
    }

    private ResponseEntity<String> proceedAddGameToGameList(long gameId, Game game, GameList gameList) {
        if (isGameAlreadyInGameList(gameId, gameList)) {
            return new ResponseEntity<>("Game already exists in " +
                    gameList.getName(), HttpStatus.NOT_ACCEPTABLE);
        }
        return assignGameToListAndReturnMessage(gameId, game, gameList);
    }

    private boolean isGameAlreadyInGameList(long gameId, GameList gameList) {
        Optional<Game> fetchedGameFromList = gameRepository.findByIdGameIdAndIdGameList(gameId, gameList);
        return fetchedGameFromList.isPresent();
    }

    private ResponseEntity<String> assignGameToListAndReturnMessage(long gameId, Game game, GameList gameList) {
        game.setId(new GameId(gameList, gameId));
        gameRepository.save(game);
        return new ResponseEntity<>("Game has been successfully added to " + gameList.getName(),
                HttpStatus.OK);
    }

    @Override
    public ResponseEntity<String> deleteGameFromGameList(String username, long listId, long gameId) {
        Optional<GameList> listInDB = gameListRepository.findById(listId);
        if (!listInDB.isPresent()) {
            return new ResponseEntity<>(GAME_LIST_NOT_EXISTS_MESSAGE, HttpStatus.NOT_ACCEPTABLE);
        }
        return checkUserAndProceedGameDeletion(username, gameId, listInDB.get());
    }

    private ResponseEntity<String> checkUserAndProceedGameDeletion(String username, long gameId, GameList listInDB) {
        if (isGameListAssignToUser(username, listInDB)) {
            return proceedDeleteGameFromGameList(gameId, listInDB);
        }
        return new ResponseEntity<>(FAILED_AUTHORIZATION_MESSAGE, HttpStatus.UNAUTHORIZED);
    }

    private ResponseEntity<String> proceedDeleteGameFromGameList(long gameId, GameList gameList) {
        Optional<Game> game = gameRepository.findByIdGameIdAndIdGameList(gameId, gameList);
        return game
                .map(value -> deleteGameAdnReturnMessage(gameList, value))
                .orElseGet(() -> new ResponseEntity<>("Game is not assign to list with given id", HttpStatus.NOT_FOUND));
    }

    private ResponseEntity<String> deleteGameAdnReturnMessage(GameList gameList, Game game) {
        gameRepository.delete(game);
        return new ResponseEntity<>("Game has been successfully deleted from " + gameList.getName(),
                HttpStatus.OK);
    }

    public static class GameListResponse {

        private GameList gameList;
        private long gameListSize;

        public GameListResponse(GameList games, long gameListSize) {
            this.gameList = games;
            this.gameListSize = gameListSize;
        }

        public GameList getGameList() {
            return gameList;
        }

        public void setGameList(GameList gameList) {
            this.gameList = gameList;
        }

        public long getGameListSize() {
            return gameListSize;
        }

        public void setGameListSize(long gameListSize) {
            this.gameListSize = gameListSize;
        }

        @Override
        public String toString() {
            return "GameListResponse{" +
                    "gameList=" + gameList +
                    ", gameListSize=" + gameListSize +
                    '}';
        }
    }
}
