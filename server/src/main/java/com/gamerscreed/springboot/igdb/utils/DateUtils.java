package com.gamerscreed.springboot.igdb.utils;

public class DateUtils {

    public final static int ONE_SEC_IN_MILLIS = 1000;

    public static long getCurrentTime() {
        return System.currentTimeMillis() / ONE_SEC_IN_MILLIS;
    }
}
