package com.gamerscreed.springboot.igdb.gamelist.repository;

import com.gamerscreed.springboot.igdb.gamelist.entity.GameList;
import com.gamerscreed.springboot.springsecurity.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GameListRepository extends JpaRepository<GameList, Long> {

    List<GameList> findAllByUser(User user);
}
