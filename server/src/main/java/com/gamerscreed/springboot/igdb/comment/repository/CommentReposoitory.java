package com.gamerscreed.springboot.igdb.comment.repository;

import com.gamerscreed.springboot.igdb.comment.entity.Comment;
import com.gamerscreed.springboot.igdb.comment.entity.CommentId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentReposoitory extends JpaRepository<Comment, CommentId> {
    List<Comment> findByIdGameId(int gameId);
}
