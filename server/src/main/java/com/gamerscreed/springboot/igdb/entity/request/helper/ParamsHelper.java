package com.gamerscreed.springboot.igdb.entity.request.helper;

import org.apache.commons.lang.StringUtils;

import java.util.Map;

public class ParamsHelper {

    private static final int MAX_ITEMS_PER_PAGE = 30;
    private static final int FIRST_PAGE = 1;

    public static void replaceGivenPage(Map<String, String> replacements) {
        String givenPage = replacements.get("givenPage");
        if (StringUtils.isNotBlank(givenPage)) {
            int page = Integer.parseInt(givenPage);
            int offset = isNotFirstPage(page) ? getOffsetForAnotherPage(page) : getFirstOffset(page);
            replacements.put("givenPage", String.valueOf(offset));
        }
    }

    private static int getOffsetForAnotherPage(int page) {
        return page * MAX_ITEMS_PER_PAGE;
    }

    private static boolean isNotFirstPage(int page) {
        return page > FIRST_PAGE;
    }

    public static String replaceParams(String inString, Map<String, String> replacements) {
        for (Map.Entry<String, String> entry : replacements.entrySet()) {
            inString = inString.replace(entry.getKey(), entry.getValue());
        }
        return inString;
    }

    private static int getFirstOffset(int page) {
        return (page == FIRST_PAGE) ? 0 : MAX_ITEMS_PER_PAGE;
    }
}
