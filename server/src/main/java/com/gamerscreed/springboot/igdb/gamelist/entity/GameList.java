package com.gamerscreed.springboot.igdb.gamelist.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.gamerscreed.springboot.igdb.gamelist.game.entity.Game;
import com.gamerscreed.springboot.springsecurity.entity.User;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "lists")
public class GameList {

    @Id
    @Column(name = "id", nullable = false)
    private long id;

    @Column(name = "name", nullable = false)
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "username", nullable = false)
    private User user;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "id.gameList")
    @JsonBackReference("games-gamelist")
    private List<Game> games;

    public GameList() {
    }

    public GameList(String name, User user) {
        this.name = name;
        this.user = user;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "GameList{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", user=" + user +
                '}';
    }
}
