package com.gamerscreed.springboot.igdb.comment.entity;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "comments")
public class Comment {

    @EmbeddedId
    private CommentId id;

    @Column(name = "body", nullable = false)
    private String body;

    @Column(name = "created_at", nullable = false)
    @JsonSerialize
    private Date createdAt;

    public Comment() {
        this.createdAt = new Date(System.currentTimeMillis());
    }

    public Comment(CommentId id, String body) {
        this.id = id;
        this.body = body;
        this.createdAt = new Date(System.currentTimeMillis());
    }

    public CommentId getId() {
        return id;
    }

    public void setId(CommentId id) {
        this.id = id;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "id=" + id +
                ", body='" + body + '\'' +
                ", createdAt=" + createdAt +
                '}';
    }
}
