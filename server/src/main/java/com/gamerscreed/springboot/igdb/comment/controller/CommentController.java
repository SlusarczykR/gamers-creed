package com.gamerscreed.springboot.igdb.comment.controller;

import com.gamerscreed.springboot.igdb.comment.entity.Comment;
import com.gamerscreed.springboot.igdb.comment.service.CommentService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin
public class CommentController {

    private CommentService commentService;

    @Autowired
    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @GetMapping("comments/{gameId}")
    public List<Comment> getCommentsByGameId(@PathVariable int gameId) {
        if (gameId != 0) {
            return commentService.getAllComments(gameId);
        }
        return new ArrayList<>();
    }

    @PostMapping("comments/{gameId}")
    public ResponseEntity<String> addCommentToGameWithId(Authentication authentication, @PathVariable int gameId,
                                                         @RequestBody String body) {
        if (areAddCommentToGameParamsValid(gameId, body)) {
            return commentService.addComment(authentication.getName(), gameId, body);
        }
        return new ResponseEntity<>("Invalid parameters", HttpStatus.NO_CONTENT);
    }

    private boolean areAddCommentToGameParamsValid(int gameId, String body) {
        return (gameId != 0) && StringUtils.isNotBlank(body);
    }
}
