package com.gamerscreed.springboot.igdb.controller;

import com.gamerscreed.springboot.igdb.entity.endpoint.game.Game;
import com.gamerscreed.springboot.igdb.entity.endpoint.game.details.GameInfo;
import com.gamerscreed.springboot.igdb.entity.endpoint.pulse.Pulse;
import com.gamerscreed.springboot.igdb.entity.endpoint.response.GameDetailsResponse;
import com.gamerscreed.springboot.igdb.service.GameAPIService;
import com.gamerscreed.springboot.igdb.service.exception.MissingFetchedDataException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.concurrent.ExecutionException;

@RestController
@CrossOrigin
@RequestMapping("/api/*")
public class GameDetailsController {

    private static final Logger LOGGER = LoggerFactory.getLogger(GameDetailsController.class.getName());

    private GameAPIService gameAPIService;

    @Autowired
    public GameDetailsController(GameAPIService gameAPIService) {
        this.gameAPIService = gameAPIService;
    }

    @GetMapping("games/header/{id}")
    public GameDetailsResponse gameHeader(@PathVariable int id)
            throws InterruptedException, ExecutionException, URISyntaxException, MissingFetchedDataException {
        return gameAPIService.getGameHeader(id);
    }

    @GetMapping("games/media/{id}")
    public Game gameMedia(@PathVariable int id)
            throws InterruptedException, ExecutionException, URISyntaxException, MissingFetchedDataException {
        return gameAPIService.getGameMedia(id);
    }

    @GetMapping("games/info/{id}")
    public GameInfo gameInfo(@PathVariable int id)
            throws InterruptedException, ExecutionException, URISyntaxException, MissingFetchedDataException {
        return gameAPIService.getGameInfoSection(id);
    }

    @GetMapping("games/similarGames/{id}")
    public List<Game> gameSimilarGames(@PathVariable int id)
            throws InterruptedException, ExecutionException, URISyntaxException, MissingFetchedDataException {
        return gameAPIService.getGameSimilarGames(id);
    }

    @GetMapping("games/pulses/{id}")
    public List<Pulse> gamePulses(@PathVariable int id)
            throws InterruptedException, ExecutionException, URISyntaxException {
        return gameAPIService.getGamePulses(id);
    }

    @GetMapping("games/pulses/{id}/{page}")
    public List<Pulse> allGamePulses(@PathVariable int id, @PathVariable("page") int page)
            throws InterruptedException, ExecutionException, URISyntaxException {
        return gameAPIService.getGamePulses(id, page);
    }
}
