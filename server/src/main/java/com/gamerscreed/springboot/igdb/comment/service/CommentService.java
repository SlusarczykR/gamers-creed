package com.gamerscreed.springboot.igdb.comment.service;

import com.gamerscreed.springboot.igdb.comment.entity.Comment;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface CommentService {

    List<Comment> getAllComments(int gameId);

    ResponseEntity<String> addComment(String username, int gameId, String body);
}
