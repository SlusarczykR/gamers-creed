package com.gamerscreed.springboot.igdb.service;

import com.gamerscreed.springboot.igdb.entity.endpoint.game.Game;
import com.gamerscreed.springboot.igdb.entity.endpoint.game.details.GameInfo;
import com.gamerscreed.springboot.igdb.entity.endpoint.game.metadata.ReleaseDate;
import com.gamerscreed.springboot.igdb.entity.endpoint.pulse.Pulse;
import com.gamerscreed.springboot.igdb.entity.endpoint.response.GameDetailsResponse;
import com.gamerscreed.springboot.igdb.service.exception.MissingFetchedDataException;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public interface GameAPIService {

    List<Game> getPopularGames() throws URISyntaxException, ExecutionException, InterruptedException;

    List<Game> getRecommendedGames() throws URISyntaxException, ExecutionException, InterruptedException;

    List<ReleaseDate> getReleaseDateByGroup(String gamesGroup) throws InterruptedException, ExecutionException, URISyntaxException;

    List<Game> getGamesByGroup(String gamesGroup) throws URISyntaxException, ExecutionException, InterruptedException;

    GameDetailsResponse getGameHeader(int id) throws URISyntaxException, ExecutionException, InterruptedException, MissingFetchedDataException;

    Game getGameMedia(int id) throws URISyntaxException, ExecutionException, InterruptedException, MissingFetchedDataException;

    List<Game> getGameSimilarGames(int id) throws URISyntaxException, ExecutionException, InterruptedException, MissingFetchedDataException;

    GameInfo getGameInfoSection(int id) throws URISyntaxException, ExecutionException, InterruptedException, MissingFetchedDataException;

    List<Pulse> getHomePulses() throws URISyntaxException, ExecutionException, InterruptedException;

    List<Pulse> getGamePulses(int id) throws InterruptedException, ExecutionException, URISyntaxException;

    List<Pulse> getGamePulses(int id, int page) throws URISyntaxException, ExecutionException, InterruptedException;

    List<Game> getSearchGameResults(String title, int page) throws URISyntaxException, ExecutionException, InterruptedException;

    List<Pulse> getLatestPulses(int page) throws URISyntaxException, ExecutionException, InterruptedException;

    List<Game> getMostPopularGames(Map<String, String> params) throws URISyntaxException, ExecutionException, InterruptedException;

    List<ReleaseDate> getRecentlyReleasedGames(Map<String, String> params) throws InterruptedException, ExecutionException, URISyntaxException;

    List<Game> getComingSoonGames(Map<String, String> params) throws URISyntaxException, ExecutionException, InterruptedException;
}
