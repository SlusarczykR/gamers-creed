package com.gamerscreed.springboot.igdb.fetcher;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import org.apache.commons.lang.StringUtils;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import static com.gamerscreed.springboot.igdb.utils.DateUtils.ONE_SEC_IN_MILLIS;

@Component
public class IgdbDataFetcher {

    private static final Logger LOGGER = LoggerFactory.getLogger(IgdbDataFetcher.class.getName());

    private static final String USER_KEY_HEADER = "user-key";
    private static final String GSON_BEAN_QUALIFIER = "igdbGson";
    private static final String EMPTY_BODY_MESSAGE = "IGDB response body is empty...";
    private static final String EMPTY_ARRAY = "[]";
    private static final String RESPONSE_BODY_MESSAGE_PREFIX = "IGDB request body: ";
    private static final int CONNECTION_TIMEOUT = 10;

    @Value("${igdb.api.key}")
    private String apiKey;
    private Gson gson;

    @Autowired
    public IgdbDataFetcher(@Qualifier(GSON_BEAN_QUALIFIER) Gson gson) {
        this.gson = gson;
    }

    @Async
    public <T> CompletableFuture<List<T>> fetchData(String url, String body, Class<T[]> itemType)
            throws URISyntaxException {

        LOGGER.info(RESPONSE_BODY_MESSAGE_PREFIX + body);

        RequestEntity<String> request = RequestEntity.post(new URI(url)).accept(MediaType.APPLICATION_JSON)
                .header(USER_KEY_HEADER, apiKey).body(body);
        ResponseEntity<String> results = new RestTemplate(getHttpComponentsClientHttpRequestFactory())
                .exchange(request, String.class);
        String responseBody = results.getBody();

        return CompletableFuture.completedFuture(parseDataFromJson(body, itemType, responseBody));
    }

    private <T> List<T> parseDataFromJson(String body, Class<T[]> itemType, String responseBody) {
        if (isResponseBodyValid(body)) {
            return parseDataAndHandleException(itemType, responseBody);
        } else {
            LOGGER.error(EMPTY_BODY_MESSAGE);
        }
        return new ArrayList<>();
    }

    private <T> List<T> parseDataAndHandleException(Class<T[]> itemType, String responseBody) {
        try {
            return Arrays.asList(gson.fromJson(responseBody, itemType));
        } catch (JsonParseException e) {
            LOGGER.error(e.getMessage());
        }
        return new ArrayList<>();
    }

    private boolean isResponseBodyValid(String body) {
        return StringUtils.isNotBlank(body) && !body.equals(EMPTY_ARRAY);
    }

    private HttpComponentsClientHttpRequestFactory getHttpComponentsClientHttpRequestFactory() {
        RequestConfig requestConfig = getRequestConfig();

        CloseableHttpClient httpClient = getConfiguredHttpClient(requestConfig);

        return getRequestFactory(httpClient);
    }

    private HttpComponentsClientHttpRequestFactory getRequestFactory(CloseableHttpClient httpClient) {
        HttpComponentsClientHttpRequestFactory requestFactory
                = new HttpComponentsClientHttpRequestFactory();
        requestFactory.setHttpClient(httpClient);
        return requestFactory;
    }

    private RequestConfig getRequestConfig() {
        return RequestConfig.custom()
                .setConnectTimeout(CONNECTION_TIMEOUT * ONE_SEC_IN_MILLIS)
                .setConnectionRequestTimeout(CONNECTION_TIMEOUT * ONE_SEC_IN_MILLIS)
                .setSocketTimeout(CONNECTION_TIMEOUT * ONE_SEC_IN_MILLIS).build();
    }

    private CloseableHttpClient getConfiguredHttpClient(RequestConfig requestConfig) {
        return HttpClients.custom()
                .setSSLHostnameVerifier(new NoopHostnameVerifier())
                .setDefaultRequestConfig(requestConfig)
                .build();
    }
}
