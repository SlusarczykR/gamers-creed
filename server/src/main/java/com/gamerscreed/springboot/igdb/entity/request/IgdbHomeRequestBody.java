package com.gamerscreed.springboot.igdb.entity.request;

public class IgdbHomeRequestBody {

    public static final String POPULAR_GAMES = "fields id, name, cover.url, first_release_date, screenshots.url, videos.video_id; "
            + "where aggregated_rating > 80 & version_parent = null; sort popularity desc; limit 30;";
    public static final String PULSES = "fields pulse_source.page.name, pulse_source.page.url, author, created_at, title, "
            + "summary, image, website.url; sort published_at desc; limit 6;";
    private static final String RECOMMENDED_GAMES = "fields id, name, cover.url, first_release_date, aggregated_rating;"
            + " where aggregated_rating > 80 & version_parent = null & first_release_date < givenDate; sort popularity desc; limit 30;";
    private static final String RECENTLY_RELEASED_GAMES = "fields date, game.name, game.cover.url; where date < "
            + "givenDate; sort date desc; limit 30;";
    private static final String COMING_SOON_GAMES = "fields date, game.name, game.cover.url; where date > givenDate;"
            + " sort date asc; limit 30;";
    private static final String MOST_POPULAR_GAMES = "fields id, name, cover.url, first_release_date, screenshots.url, videos.video_id;"
            + " where version_parent = null & first_release_date > givenDate; sort popularity desc; limit 30;";

    public static String getBodyWithGivenDate(long date, String field) {
        switch (field) {
            case "RECOMMENDED_GAMES":
                return RECOMMENDED_GAMES.replace("givenDate", String.valueOf(date));
            case "RECENTLY_RELEASED_GAMES":
                return RECENTLY_RELEASED_GAMES.replace("givenDate", String.valueOf(date));
            case "COMING_SOON_GAMES":
                return COMING_SOON_GAMES.replace("givenDate", String.valueOf(date));
            case "MOST_POPULAR_GAMES":
                return MOST_POPULAR_GAMES.replace("givenDate", String.valueOf(date));
            default:
                return "";
        }
    }
}
