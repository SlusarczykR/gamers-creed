package com.gamerscreed.springboot.igdb.entity.request;

import com.gamerscreed.springboot.igdb.entity.request.helper.ParamsHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Map;

import static com.gamerscreed.springboot.igdb.utils.DateUtils.ONE_SEC_IN_MILLIS;

public class IgdbItemsWithOffsetRequestBody {

    private static final Logger LOGGER = LoggerFactory.getLogger(IgdbItemsWithOffsetRequestBody.class.getName());

    private static final String YEAR_UNIT = "YEAR";
    private static final String DAY_UNIT = "DAY";

    private static final String GAMES_WITH_TITLE = "fields id, name, cover.url; where name ~ *\"givenTitle\"*; "
            + "sort popularity desc; limit 30; offset givenPage;";
    private static final String MOST_ANTICIPATED = "fields id, name, cover.url, first_release_date, videos.video_id;"
            + " where version_parent = null & first_release_date > startDate; sort popularity desc; limit 30; offset givenPage;";
    private static final String RECENTLY_RELEASED_GAMES = "fields date, game.name, game.cover.url, game.first_release_date; where date < "
            + "startDate & game.platforms = givenPlatform; sort date desc; limit 30; offset givenPage;";
    private static final String COMING_SOON_GAMES = "fields name, cover.url, first_release_date; where first_release_date > "
            + "startDate & first_release_date < endDate & platforms = givenPlatform; sort first_release_date asc; limit 30; offset givenPage;";
    private static final String LATEST_PULSES = "fields pulse_source.page.name, pulse_source.page.url, author, created_at, title, "
            + "summary, image, website.url; sort published_at desc; limit 30; offset givenPage;";

    public static String getBodyWithGivenPage(String request, Map<String, String> replacements) {
        ParamsHelper.replaceGivenPage(replacements);
        switch (request) {
            case "GAMES_WITH_TITLE":
                return ParamsHelper.replaceParams(GAMES_WITH_TITLE, replacements);
            case "MOST_ANTICIPATED":
                return ParamsHelper.replaceParams(MOST_ANTICIPATED, replacements);
            case "RECENTLY_RELEASED_GAMES":
                return ParamsHelper.replaceParams(RECENTLY_RELEASED_GAMES, replacements);
            case "COMING_SOON_GAMES":
                return ParamsHelper.replaceParams(COMING_SOON_GAMES, replacements);
            case "LATEST_PULSES":
                return ParamsHelper.replaceParams(LATEST_PULSES, replacements);
            default:
                return "";
        }
    }

    public static String getBodyWithGivenPeriodAndPage(String request, Map<String, String> params) {
        long date = Long.valueOf(params.get("startDate"));
        LocalDate startDate = Instant.ofEpochMilli(date * ONE_SEC_IN_MILLIS)
                .atZone(ZoneId.systemDefault()).toLocalDate();
        String period = params.get("period");

        switch (period) {
            case "week":
                params.put("endDate", String.valueOf(addDaysToDate(startDate, 7, DAY_UNIT)));
                return getBodyWithGivenPage(request, params);
            case "twoWeeks":
                params.put("startDate", String.valueOf(addDaysToDate(startDate, 7, DAY_UNIT)));
                params.put("endDate", String.valueOf(addDaysToDate(startDate, 14, DAY_UNIT)));
                return getBodyWithGivenPage(request, params);
            case "all":
                params.put("endDate", String.valueOf(addDaysToDate(startDate, 10, YEAR_UNIT)));
                return getBodyWithGivenPage(request, params);
            default:
                return "";
        }
    }

    private static long addDaysToDate(LocalDate startDate, int i, String dateUnit) {
        switch (dateUnit) {
            case YEAR_UNIT:
                return startDate.plusYears(10).atStartOfDay(ZoneId.systemDefault()).toEpochSecond();
            case DAY_UNIT:
                return startDate.plusDays(i).atStartOfDay(ZoneId.systemDefault()).toEpochSecond();
            default:
                return 0L;
        }
    }
}
