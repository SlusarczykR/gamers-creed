package com.gamerscreed.springboot.igdb.config.gson.mapper;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.util.logging.Logger;

public class IgdbTypeAdapterFactory implements TypeAdapterFactory {

    private static final Logger LOGGER = Logger.getLogger(IgdbTypeAdapterFactory.class.getName());

    public static <T> boolean isInstanceOf(Class<T> clazz, Class<T> targetClass) {
        return clazz.isInstance(targetClass);
    }

    public <T> TypeAdapter<T> create(Gson gson, final TypeToken<T> type) {

        final TypeAdapter<T> delegate = gson.getDelegateAdapter(this, type);
        final TypeAdapter<JsonElement> elementAdapter = gson.getAdapter(JsonElement.class);

        return new TypeAdapter<T>() {

            public void write(JsonWriter out, T value) throws IOException {
                delegate.write(out, value);
            }

            public T read(JsonReader in) throws IOException {

                String typeOfT = type.getType().getTypeName();

                JsonElement jsonElement = elementAdapter.read(in);

                if (jsonElement.isJsonPrimitive()) {
                    JsonPrimitive jsonPrimitive = jsonElement.getAsJsonPrimitive();
                    if (jsonPrimitive.isNumber()) {
                        if (!typeOfT.equals("int") && !typeOfT.equals("double") && !typeOfT.equals("java.lang.Long")) {
                            LOGGER.info("JsonElement's type is invalid - expected object but was number...");
                            jsonElement = new JsonParser().parse(jsonElement.toString().replace(jsonPrimitive.getAsNumber().toString(), ""));
                        }
                    }
                }

                return delegate.fromJsonTree(jsonElement);
            }
        }.nullSafe();
    }
}