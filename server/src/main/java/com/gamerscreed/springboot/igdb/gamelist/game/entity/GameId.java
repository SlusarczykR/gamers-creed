package com.gamerscreed.springboot.igdb.gamelist.game.entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.gamerscreed.springboot.igdb.gamelist.entity.GameList;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class GameId implements Serializable {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "list_id", nullable = false)
    @JsonSerialize
    private GameList gameList;

    @Column(name = "igdb_game_id", nullable = false)
    @JsonSerialize
    private Long gameId;

    public GameId() {
    }

    public GameId(GameList gameList, Long gameId) {
        this.gameList = gameList;
        this.gameId = gameId;
    }

    public GameList getGameList() {
        return gameList;
    }

    public void setGameList(GameList gameList) {
        this.gameList = gameList;
    }

    public Long getGameId() {
        return gameId;
    }

    public void setGameId(Long gameId) {
        this.gameId = gameId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GameId)) return false;
        GameId gameId1 = (GameId) o;
        return Objects.equals(gameList, gameId1.gameList) &&
                Objects.equals(gameId, gameId1.gameId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(gameList, gameId);
    }

    @Override
    public String toString() {
        return "GameId{" +
                "gameList=" + gameList +
                ", gameId=" + gameId +
                '}';
    }
}
