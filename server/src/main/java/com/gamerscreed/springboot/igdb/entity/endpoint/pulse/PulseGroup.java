package com.gamerscreed.springboot.igdb.entity.endpoint.pulse;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PulseGroup {

	private int game;
	private List<Pulse> pulses;

	public PulseGroup() {
		this.game = 0;
		this.pulses = new ArrayList<>();
	}

	public int getGame() {
		return game;
	}

	public void setGame(int game) {
		this.game = game;
	}

	public List<Pulse> getPulses() {
		return pulses;
	}

	public void setPulses(List<Pulse> pulses) {
		this.pulses = pulses;
	}

	@Override
	public String toString() {
		return "PulseGroup [game=" + game + ", pulses=" + pulses + "]";
	}

}
