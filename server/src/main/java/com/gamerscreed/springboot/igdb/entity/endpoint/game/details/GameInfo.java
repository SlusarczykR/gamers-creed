package com.gamerscreed.springboot.igdb.entity.endpoint.game.details;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.gamerscreed.springboot.igdb.entity.endpoint.game.Game;
import com.gamerscreed.springboot.igdb.entity.endpoint.game.NameEndpoint;
import com.gamerscreed.springboot.igdb.entity.endpoint.game.UrlEndpoint;
import com.gamerscreed.springboot.igdb.entity.endpoint.game.media.Video;
import com.gamerscreed.springboot.igdb.entity.endpoint.game.metadata.InvolvedCompanie;
import com.gamerscreed.springboot.igdb.entity.endpoint.game.metadata.SimilarGame;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GameInfo {

    private Long first_release_date;
    private List<NameEndpoint> player_perspectives;
    private List<NameEndpoint> game_engines;
    private List<NameEndpoint> genres;
    private List<UrlEndpoint> screenshots;
    private List<Video> videos;
    private List<NameEndpoint> platforms;
    private List<SimilarGame> similar_games;
    private List<NameEndpoint> themes;
    private List<InvolvedCompanie> involved_companies;
    private List<NameEndpoint> game_modes;
    private List<Game> dlcs;
    private List<Game> expansions;

    public GameInfo() {
        this.first_release_date = 0L;
        this.player_perspectives = new ArrayList<>();
        this.game_engines = new ArrayList<>();
        this.genres = new ArrayList<>();
        this.screenshots = new ArrayList<>();
        this.videos = new ArrayList<>();
        this.platforms = new ArrayList<>();
        this.similar_games = new ArrayList<>();
        this.themes = new ArrayList<>();
        this.involved_companies = new ArrayList<>();
        this.game_modes = new ArrayList<>();
        this.dlcs = new ArrayList<>();
        this.expansions = new ArrayList<>();
    }

    public Long getFirst_release_date() {
        return first_release_date;
    }

    public void setFirst_release_date(Long first_release_date) {
        this.first_release_date = first_release_date;
    }

    public List<NameEndpoint> getPlayer_perspectives() {
        return player_perspectives;
    }

    public void setPlayer_perspectives(List<NameEndpoint> player_perspectives) {
        this.player_perspectives = player_perspectives;
    }

    public List<NameEndpoint> getGame_engines() {
        return game_engines;
    }

    public void setGame_engines(List<NameEndpoint> game_engines) {
        this.game_engines = game_engines;
    }

    public List<NameEndpoint> getGenres() {
        return genres;
    }

    public void setGenres(List<NameEndpoint> genres) {
        this.genres = genres;
    }

    public List<UrlEndpoint> getScreenshots() {
        return screenshots;
    }

    public void setScreenshots(List<UrlEndpoint> screenshots) {
        this.screenshots = screenshots;
    }

    public List<Video> getVideos() {
        return videos;
    }

    public void setVideos(List<Video> videos) {
        this.videos = videos;
    }

    public List<NameEndpoint> getPlatforms() {
        return platforms;
    }

    public void setPlatforms(List<NameEndpoint> platforms) {
        this.platforms = platforms;
    }

    public List<SimilarGame> getSimilar_games() {
        return similar_games;
    }

    public void setSimilar_games(List<SimilarGame> similar_games) {
        this.similar_games = similar_games;
    }

    public List<NameEndpoint> getThemes() {
        return themes;
    }

    public void setThemes(List<NameEndpoint> themes) {
        this.themes = themes;
    }

    public List<InvolvedCompanie> getInvolved_companies() {
        return involved_companies;
    }

    public void setInvolved_companies(List<InvolvedCompanie> involved_companies) {
        this.involved_companies = involved_companies;
    }

    public List<NameEndpoint> getGame_modes() {
        return game_modes;
    }

    public void setGame_modes(List<NameEndpoint> game_modes) {
        this.game_modes = game_modes;
    }

    public List<Game> getDlcs() {
        return dlcs;
    }

    public void setDlcs(List<Game> dlcs) {
        this.dlcs = dlcs;
    }

    public List<Game> getExpansions() {
        return expansions;
    }

    public void setExpansions(List<Game> expansions) {
        this.expansions = expansions;
    }

    @Override
    public String toString() {
        return "GameInfo{" +
                "first_release_date=" + first_release_date +
                ", player_perspectives=" + player_perspectives +
                ", game_engines=" + game_engines +
                ", genres=" + genres +
                ", screenshots=" + screenshots +
                ", videos=" + videos +
                ", platforms=" + platforms +
                ", similar_games=" + similar_games +
                ", themes=" + themes +
                ", involved_companies=" + involved_companies +
                ", game_modes=" + game_modes +
                ", dlcs=" + dlcs +
                ", expansions=" + expansions +
                '}';
    }
}
