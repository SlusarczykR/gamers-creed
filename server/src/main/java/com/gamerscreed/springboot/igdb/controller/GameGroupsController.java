package com.gamerscreed.springboot.igdb.controller;

import com.gamerscreed.springboot.igdb.entity.endpoint.game.Game;
import com.gamerscreed.springboot.igdb.entity.endpoint.game.metadata.ReleaseDate;
import com.gamerscreed.springboot.igdb.service.GameAPIService;
import com.gamerscreed.springboot.igdb.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.logging.Logger;

@RestController
@CrossOrigin
@RequestMapping("/api/*")
public class GameGroupsController {

    private static final Logger LOGGER = Logger.getLogger(GameGroupsController.class.getName());

    private GameAPIService gameAPIService;

    @Autowired
    public GameGroupsController(GameAPIService gameAPIService) {
        this.gameAPIService = gameAPIService;
    }

    @GetMapping("games/recentlyReleased/platform/{platform}/{page}")
    public List<ReleaseDate> recentlyReleased(@PathVariable("platform") String platform, @PathVariable("page") int page)
            throws InterruptedException, URISyntaxException, ExecutionException {

        Map<String, String> params = new HashMap<>();
        params.put("givenPage", String.valueOf(page));
        params.put("startDate", String.valueOf(DateUtils.getCurrentTime()));
        params.put("givenPlatform", String.valueOf(platform));

        return gameAPIService.getRecentlyReleasedGames(params);
    }


    @GetMapping("games/mostPopular/{page}")
    public List<Game> mostPopularGames(@PathVariable("page") int page) throws InterruptedException, URISyntaxException,
            ExecutionException {

        Map<String, String> params = new HashMap<>();
        params.put("givenPage", String.valueOf(page));
        params.put("startDate", String.valueOf(DateUtils.getCurrentTime()));

        return gameAPIService.getMostPopularGames(params);
    }

    @GetMapping("games/comingSoon/{period}/{platform}/{page}")
    public List<Game> comingSoonGames(@PathVariable("period") String period, @PathVariable("platform") String platform,
                                      @PathVariable("page") int page) throws InterruptedException, URISyntaxException,
            ExecutionException {

        Map<String, String> params = new HashMap<>();
        params.put("givenPage", String.valueOf(page));
        params.put("startDate", String.valueOf(DateUtils.getCurrentTime()));
        params.put("period", period);
        params.put("givenPlatform", String.valueOf(platform));

        return gameAPIService.getComingSoonGames(params);
    }
}
