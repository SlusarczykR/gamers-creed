package com.gamerscreed.springboot.igdb.entity.endpoint.game;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UrlEndpoint {

	private String url;

	public UrlEndpoint() {
		this.url = "";
	}

	public UrlEndpoint(String url) {
		this.url = url;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String toString() {
		return "UrlEndpoint [url=" + url + "]";
	}

}
