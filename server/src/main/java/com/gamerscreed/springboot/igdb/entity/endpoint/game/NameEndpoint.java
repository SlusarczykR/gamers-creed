package com.gamerscreed.springboot.igdb.entity.endpoint.game;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class NameEndpoint {

	private String name;

	public NameEndpoint() {
		this.name = "";
	}

	public NameEndpoint(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "NameEndpoint [name=" + name + "]";
	}

}
