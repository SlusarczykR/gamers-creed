package com.gamerscreed.springboot.igdb.entity.request;

public class IgdbEndpoints {
    public static final String GAMES = "https://api-v3.igdb.com/games";
    public static final String RELEASE_DATES = "https://api-v3.igdb.com/release_dates";
    public static final String PULSES = "https://api-v3.igdb.com/pulses";
    public static final String PULSE_GROUPS = "https://api-v3.igdb.com/pulse_groups";
}
