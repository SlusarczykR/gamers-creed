package com.gamerscreed.springboot.igdb.entity.endpoint.response;

import com.gamerscreed.springboot.igdb.entity.endpoint.game.Game;

public class GameDetailsResponse {

    private Game game;
    private String headerUrl;
    private String releaseDate;

    public GameDetailsResponse() {
    }

    public GameDetailsResponse(Game game, String headerUrl, String releaseDate) {
        this.game = game;
        this.headerUrl = headerUrl;
        this.releaseDate = releaseDate;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public String getHeaderUrl() {
        return headerUrl;
    }

    public void setHeaderUrl(String headerUrl) {
        this.headerUrl = headerUrl;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    @Override
    public String toString() {
        return "GameDetailsResponse{" +
                "game=" + game +
                ", headerUrl='" + headerUrl + '\'' +
                ", releaseDate='" + releaseDate + '\'' +
                '}';
    }
}
