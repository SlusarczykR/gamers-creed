package com.gamerscreed.springboot.igdb.parser;

import com.gamerscreed.springboot.igdb.entity.endpoint.game.Game;
import com.gamerscreed.springboot.igdb.entity.endpoint.game.UrlEndpoint;
import com.gamerscreed.springboot.igdb.entity.endpoint.game.media.Cover;
import com.gamerscreed.springboot.igdb.entity.endpoint.game.metadata.ReleaseDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static com.gamerscreed.springboot.igdb.constant.ImageSizes.THUMB;

@Component
public class IgdbDataParser {

    private static final Logger LOGGER = LoggerFactory.getLogger(IgdbDataParser.class.getName());

    public <T> List<T> truncateGamesList(List<T> dataList, int from, int to) {
        return isCompartmentValid(dataList, from, to) ? getGameSublist(dataList, from, to) : dataList;
    }

    private <T> boolean isCompartmentValid(List<T> dataList, int from, int to) {
        return !dataList.isEmpty() && from >= 0 && to > 0;
    }

    private <T> List<T> getGameSublist(List<T> dataList, int from, int to) {
        return dataList.size() >= to ? dataList.subList(from, to) : dataList;
    }

    public List<ReleaseDate> distinctGamesByTitle(List<ReleaseDate> releaseDates) {
        return releaseDates.stream()
                .filter(Objects::nonNull)
                .filter(distinctByKey(this::getReleaseDateGameName))
                .collect(Collectors.toList());
    }

    private <T> Predicate<T> distinctByKey(
            Function<? super T, ?> keyExtractor) {
        Map<Object, Boolean> seen = new ConcurrentHashMap<>();
        return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }

    private String getReleaseDateGameName(ReleaseDate releaseDate) {
        String name = null;
        if (releaseDate != null) {
            Game game = releaseDate.getGame();
            if (game != null) {
                name = game.getName();
            }
        }
        return name;
    }

    public void changeGameCoverSize(Game game, String coverSize) {
        if (game != null) {
            Cover cover = game.getCover();
            if (cover != null) {
                String url = cover.getUrl();
                if (url != null) {
                    game.setCover(new Cover(url.replaceAll(THUMB, coverSize)));
                }
            }
        }
    }

    public void changeScreenshotsSize(List<UrlEndpoint> entryList, String coverSize) {
        if (!entryList.isEmpty()) {
            entryList.removeAll(Collections.singleton(null));
            entryList.forEach(screenshot -> setScreenshotSize(coverSize, screenshot));
        }
    }

    private void setScreenshotSize(String coverSize, UrlEndpoint screenshot) {
        String url = screenshot.getUrl();
        if (url != null) {
            screenshot.setUrl(url.replaceAll(THUMB, coverSize));
        }
    }

    public void changeSimilarGamesCoverSize(List<Game> entryList, String coverSize) {
        entryList.forEach(game -> changeGameCoverSize(game, coverSize));
    }
}
