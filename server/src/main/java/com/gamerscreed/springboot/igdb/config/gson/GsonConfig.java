package com.gamerscreed.springboot.igdb.config.gson;

import com.gamerscreed.springboot.igdb.config.gson.mapper.IgdbTypeAdapterFactory;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class GsonConfig {

    @Bean(name = "igdbGson")
    public Gson getIgdbGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapterFactory(new IgdbTypeAdapterFactory());
        return gsonBuilder.serializeNulls().create();
    }
}
