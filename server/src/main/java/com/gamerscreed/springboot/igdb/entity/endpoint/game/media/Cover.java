package com.gamerscreed.springboot.igdb.entity.endpoint.game.media;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Cover {

	private String url;
	private int game;

	public Cover() {
		this.url = "";
		this.game = 0;
	}

	public Cover(String url) {
		this.url = url;
		this.game = 0;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getGame() {
		return game;
	}

	public void setGame(int game) {
		this.game = game;
	}

	@Override
	public String toString() {
		return "GameCover [url=" + url + ", game=" + game + "]";
	}

}
