package com.gamerscreed.springboot.springsecurity.controller;

import com.gamerscreed.springboot.springsecurity.entity.UserDto;
import com.gamerscreed.springboot.springsecurity.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class SecurityController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SecurityController.class.getName());

    private static final String MISSING_CREDENTIALS_MESSAGE = "Missing username or password";

    private UserService userService;

    @Autowired
    public SecurityController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("users")
    public List<UserDto> getUsers() {
        return userService.getAllUsers();
    }

    @PostMapping("users/{username}/{password}")
    public ResponseEntity<String> addUser(@PathVariable String username, @PathVariable String password) {
        if (areCredentialsValid(username, password)) {
            return userService.addUserToDb(username, password);
        }
        return new ResponseEntity<>(MISSING_CREDENTIALS_MESSAGE, HttpStatus.NO_CONTENT);
    }

    private boolean areCredentialsValid(String username, String password) {
        return StringUtils.isNotBlank(username) && StringUtils.isNotBlank(password);
    }
}
