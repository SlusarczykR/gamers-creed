package com.gamerscreed.springboot.springsecurity.service;

import com.gamerscreed.springboot.springsecurity.entity.UserDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface UserService {
    List<UserDto> getAllUsers();

    ResponseEntity<String> addUserToDb(String username, String password);
}
