package com.gamerscreed.springboot.springsecurity.exception;

public class UserAlreadyExsistException extends Exception {

    public UserAlreadyExsistException() {
        super();
    }

    public UserAlreadyExsistException(String message) {
        super(message);
    }
}
