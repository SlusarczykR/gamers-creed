package com.gamerscreed.springboot.springsecurity.config.oauth.server;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;

@Configuration
@EnableResourceServer
public class ResourceServerOAuthConfig extends ResourceServerConfigurerAdapter {

    private static final String API_ENDPOINTS = "/api/**";
    private static final String USER_ENDPOINTS = "/users/**";
    private static final String LIST_ENDPOINTS = "/lists/**";
    private static final String COMMENT_ENDPOINTS = "/comments/**";

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .authorizeRequests()
                .antMatchers(API_ENDPOINTS)
                .permitAll()
                .and()
                .authorizeRequests()
                .antMatchers(USER_ENDPOINTS)
                .permitAll()
                .and()
                .authorizeRequests()
                .antMatchers(GET, USER_ENDPOINTS)
                .authenticated()
                .and()
                .authorizeRequests()
                .antMatchers(LIST_ENDPOINTS)
                .authenticated()
                .and()
                .authorizeRequests()
                .antMatchers(POST, COMMENT_ENDPOINTS)
                .authenticated();
    }
}
