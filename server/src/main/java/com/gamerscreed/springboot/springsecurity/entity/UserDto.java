package com.gamerscreed.springboot.springsecurity.entity;

public class UserDto {

    private String username;
    private int enabled;

    public UserDto() {
    }

    public UserDto(String username) {
        this.username = username;
        this.enabled = 1;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getEnabled() {
        return enabled;
    }

    public void setEnabled(int enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return "UserDto{" +
                "username='" + username + '\'' +
                ", enabled=" + enabled +
                '}';
    }
}
