package com.gamerscreed.springboot.springsecurity.config.oauth.enhancer;

import com.gamerscreed.springboot.springsecurity.entity.Authority;
import com.gamerscreed.springboot.springsecurity.entity.User;
import com.gamerscreed.springboot.springsecurity.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.*;
import java.util.logging.Logger;

@Component
public class CustomTokenEnhancer implements TokenEnhancer {

    private static final Logger LOGGER = Logger.getLogger(CustomTokenEnhancer.class.getName());

    @Autowired
    private UserRepository userRepository;

    @Override
    @Transactional
    public OAuth2AccessToken enhance(
            OAuth2AccessToken accessToken,
            OAuth2Authentication authentication) {
        Map<String, Object> additionalInfo = new HashMap<>();

        Optional<User> user = userRepository.findById(authentication.getName());
        LOGGER.info("@User: " + user);
        if (user.isPresent()) {
            User usr = user.get();
            additionalInfo.put("username", usr.getUsername());
            List<Authority> authorities = new ArrayList<>(usr.getAuthorities());
            if (!authorities.isEmpty()) {
                additionalInfo.put("role", authorities.get(0).getAuthority());
            }
        }
        ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(
                additionalInfo);
        return accessToken;
    }
}