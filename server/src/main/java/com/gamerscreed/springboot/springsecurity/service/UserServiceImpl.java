package com.gamerscreed.springboot.springsecurity.service;

import com.gamerscreed.springboot.springsecurity.entity.Authority;
import com.gamerscreed.springboot.springsecurity.entity.User;
import com.gamerscreed.springboot.springsecurity.entity.UserDto;
import com.gamerscreed.springboot.springsecurity.exception.UserAlreadyExsistException;
import com.gamerscreed.springboot.springsecurity.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

@Service
public class UserServiceImpl implements UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class.getName());

    private static final String USER_ROLE = "ROLE_USER";
    private static final int ENABLED = 1;
    private static final String SUCCESSFULLY_CREATED_MESSAGE = "User has been successfully created";
    private static final String ALREADY_EXISTS_MESSAGE = "User with given username already exists";

    private UserRepository userRepository;
    private ModelMapper modelMapper;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, ModelMapper modelMapper, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.modelMapper = modelMapper;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public List<UserDto> getAllUsers() {
        return userRepository.findAll().stream()
                .map(this::convertToDto)
                .collect(toList());
    }

    @Override
    public ResponseEntity<String> addUserToDb(String username, String password) {
        Optional<User> userInDB = userRepository.findById(username);
        if (!userInDB.isPresent()) {
            return createUserAndSendStatus(username, password);
        } else {
            return getUserAlreadyExistErrorMessage();
        }
    }

    private ResponseEntity<String> createUserAndSendStatus(String username, String password) {
        User newUser = new User(username, passwordEncoder.encode(password), ENABLED);
        newUser.addAuthority(new Authority(newUser, USER_ROLE));
        userRepository.save(newUser);
        return new ResponseEntity<>(SUCCESSFULLY_CREATED_MESSAGE, HttpStatus.OK);
    }

    private ResponseEntity<String> getUserAlreadyExistErrorMessage() {
        try {
            throw new UserAlreadyExsistException(ALREADY_EXISTS_MESSAGE);
        } catch (UserAlreadyExsistException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return new ResponseEntity<>(ALREADY_EXISTS_MESSAGE, HttpStatus.NOT_ACCEPTABLE);
    }

    private UserDto convertToDto(User user) {
        UserDto userDto = modelMapper.map(user, UserDto.class);
        userDto.setUsername(user.getUsername());
        userDto.setEnabled(user.getEnabled());
        return userDto;
    }
}
