package com.gamerscreed.springboot.springsecurity.repository;

import com.gamerscreed.springboot.springsecurity.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, String> {
}
